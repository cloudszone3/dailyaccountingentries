<?php

use App\Http\Controllers\AccountingNotebookController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\InvoicesDetaillsController;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\InvoicesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\InvoiceAttachmentsTableController;
use App\Http\Controllers\Invoices_Report;
use App\Http\Controllers\Customers_Report;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LiquidationStatusController;
use App\Http\Controllers\MyAccountantsController;
use App\Http\Controllers\SendToController;


Route::get('/', function () {
     return view('auth.login'); });

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->middleware('auth')->name('home');

Route::post('search_by_date', [HomeController::class, 'search_by_date'])->middleware('auth')->name('home.search.invoices');


Route::resource('invoice_attachments_table', InvoiceAttachmentsTableController::class);

Route::controller(InvoicesDetaillsController::class)->group( function () {
        Route::get('/InvoicesDetails/{id}', 'edit');

        Route::get('download/{invoice_number}/{file_name}', 'get_file');

        Route::get('View_file/{invoice_number}/{file_name}', 'open_file');

        Route::post('delete_file', 'destroy')->name('delete_file');
        }
);

Route::resource('invoices',InvoicesController::class);
// Route::get('/edit_getfile/{id}', [InvoicesController::class,'getfile']);

Route::controller(InvoicesController::class)->group( function () {


        Route::get('/edit_invoice/{id}', 'edit');

        Route::get('/Status_show/{id}', 'show')->name('Status_show');

        Route::post('/Status_Update/{id}', 'Status_Update')->name('Status_Update');

        Route::get('Invoice_Paid','Invoice_Paid');

        Route::get('Invoice_UnPaid','Invoice_UnPaid');

        Route::get('Print_invoice/{id}','Print_invoice');

        Route::get('export_invoices','export');


    }
);

Route::controller(Invoices_Report::class)->group( function () {

        Route::get('invoices_report','index');

        Route::post('Search_invoices','Search_invoices');

        Route::post('export_report_pdf','export_report_pdf')->name('export_report_pdf.pdf');

        Route::get('paid_invoices','paid_invoices');

        Route::get('unpaid_invoices','unpaid_invoices');

    }
);

Route::post('Search_customers', [Customers_Report::class,'Search_customers']);

Route::group(['middleware' => ['auth']], function() {

    Route::resource('roles',RoleController::class);

    Route::resource('users',UserController::class);
});

///////////////////////////////////Start AccountingNotebook Route/////////////////////////////////////
Route::group(['prefix' => 'AccountingNotebook'] , function() {


    // Route::get('/index' , [AccountingNotebookController::class , 'index'])->name('AccountingNotebook.index') ;

    Route::get('/create' , [AccountingNotebookController::class , 'create'])->name('AccountingNotebook.create') ;

    Route::post('/store' , [AccountingNotebookController::class , 'store'])->name('AccountingNotebook.store') ;

    Route::get('/edit/{id}' , [AccountingNotebookController::class , 'edit'])->name('AccountingNotebook.edit') ;

    Route::post('/update' , [AccountingNotebookController::class , 'update'])->name('AccountingNotebook.update') ;

    Route::get('/delete/{id}' , [AccountingNotebookController::class , 'destroy'])->name('AccountingNotebook.delete') ;

    Route::post('search', [AccountingNotebookController::class , 'search_in_accounting'])->name('search_in_accounting');


    // Start Box
    // Route::get('/' , [LanguageController::class , 'index'])->name('admin.language.index') ;

    Route::group([ 'prefix' =>   'box'] , function()    {

        Route::get('/' , [BoxController::class , 'index'])->name('AccountingNotebook.box.index') ;

        Route::get('/create' , [BoxController::class , 'create'])->name('AccountingNotebook.box.create') ;

        Route::post('store' , [BoxController::class , 'store'])->name('AccountingNotebook.box.store') ;

        Route::get('edit/{id}' , [BoxController::class , 'edit'])->name('AccountingNotebook.box.edit') ;

        Route::post('update' , [BoxController::class , 'update'])->name('AccountingNotebook.box.update') ;

        Route::get('/delete/{id}' , [BoxController::class , 'destroy'])->name('AccountingNotebook.box.delete') ;
    }) ;
    // End Box


    // Start SendTo
    Route::group([ 'prefix' =>   'SendTo'] , function()    {

        Route::get('/' , [SendToController::class , 'index'])->name('AccountingNotebook.SendTo.index') ;

        Route::get('/create' , [SendToController::class , 'create'])->name('AccountingNotebook.SendTo.create') ;

        Route::post('store' , [SendToController::class , 'store'])->name('AccountingNotebook.SendTo.store') ;

        Route::get('edit/{id}' , [SendToController::class , 'edit'])->name('AccountingNotebook.SendTo.edit') ;

        Route::post('update' , [SendToController::class , 'update'])->name('AccountingNotebook.SendTo.update') ;

        Route::get('/delete/{id}' , [SendToController::class , 'destroy'])->name('AccountingNotebook.SendTo.delete') ;
    }) ;
    // End SendTo


    // Start LiquidationStatus
    Route::group([ 'prefix' =>   'LiquidationStatus'] , function()    {

        Route::get('/' , [LiquidationStatusController::class , 'index'])->name('AccountingNotebook.LiquidationStatus.index') ;

        Route::get('/create' , [LiquidationStatusController::class , 'create'])->name('AccountingNotebook.LiquidationStatus.create') ;

        Route::post('store' , [LiquidationStatusController::class , 'store'])->name('AccountingNotebook.LiquidationStatus.store') ;

        Route::get('edit/{id}' , [LiquidationStatusController::class , 'edit'])->name('AccountingNotebook.LiquidationStatus.edit') ;

        Route::post('update' , [LiquidationStatusController::class , 'update'])->name('AccountingNotebook.LiquidationStatus.update') ;

        Route::get('/delete/{id}' , [LiquidationStatusController::class , 'destroy'])->name('AccountingNotebook.LiquidationStatus.delete') ;
    }) ;
})  ;
    // #end LiquidationStatus

     // Start Company
Route::prefix('company')->name('company.')->group(function () {

    Route::get('/', [CompanyController::class,'index'])->name('allcompany');

    Route::get('/add-Company', [CompanyController::class,'create'])->name('addCompany');

    Route::post('/add-Company', [CompanyController::class,'store'])->name('addCompany');

    Route::get('/edit-Company/{id}', [CompanyController::class,'edit'])->name('editCompany');

    Route::post('/update-company', [CompanyController::class,'update'])->name('updateCompany');

    Route::get('/delete-company/{id}', [CompanyController::class,'destroy'])->name('destroycompany');
});
    // #end Company

         // Start dftar
    Route::get('/myaccountants', [MyAccountantsController::class,'index'])->name('myaccountants');

    Route::post('/add-myaccountants', [MyAccountantsController::class,'store'])->name('myaccountants.store');

    Route::get('/edit-myaccountants/{id}', [MyAccountantsController::class,'edit'])->name('myaccountants.edit');

    Route::post('/update-myaccountants', [MyAccountantsController::class,'update'])->name('myaccountants.update');

    Route::get('/delete-myaccountants/{id}', [MyAccountantsController::class,'destroy'])->name('myaccountants.delete');

    Route::get('/myaccountants/accountingnotebook/{id}', [MyAccountantsController::class,'create'])->name('addmyaccountants');

    Route::get('/allaccountingnotebook/{id}', [MyAccountantsController::class,'show'])->name('allaccountingnotebook.show');

    // #end dftar
