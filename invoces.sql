-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2023 at 09:44 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoces`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounting_notebooks`
--

CREATE TABLE `accounting_notebooks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Constraint` varchar(50) NOT NULL,
  `InvoiceNumber` varchar(50) NOT NULL,
  `display_number` varchar(50) NOT NULL,
  `DateOfRegistration` date NOT NULL,
  `Creditor` double(8,2) NOT NULL,
  `Debtor` double(8,2) NOT NULL,
  `companies_id` bigint(20) UNSIGNED NOT NULL,
  `Box_id` bigint(20) UNSIGNED NOT NULL,
  `SendTo_id` bigint(20) UNSIGNED NOT NULL,
  `LiquidationStatus_id` bigint(20) UNSIGNED NOT NULL,
  `ReferenceNumber` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounting_notebooks`
--

INSERT INTO `accounting_notebooks` (`id`, `Constraint`, `InvoiceNumber`, `display_number`, `DateOfRegistration`, `Creditor`, `Debtor`, `companies_id`, `Box_id`, `SendTo_id`, `LiquidationStatus_id`, `ReferenceNumber`, `photo`, `created_at`, `updated_at`) VALUES
(23, '888', '', '', '2023-12-25', 8888.00, 0.00, 1, 1, 1, 1, '', NULL, '2023-12-25 14:59:59', '2023-12-25 14:59:59'),
(24, '888', '', '', '2023-12-25', 0.00, 888.00, 1, 1, 1, 1, '', NULL, '2023-12-25 15:00:59', '2023-12-25 15:00:59'),
(25, '777', '', '', '2023-12-25', 777.00, 777.00, 1, 1, 1, 1, '', NULL, '2023-12-25 15:01:15', '2023-12-25 15:01:15'),
(26, '888', '', '', '2023-12-25', 888.00, 0.00, 1, 1, 1, 1, '', NULL, '2023-12-25 15:04:36', '2023-12-25 15:04:36'),
(27, '98788', '364', '893', '1986-05-25', 98788.00, 0.00, 1, 1, 1, 1, '681', NULL, '2023-12-25 17:15:22', '2023-12-25 17:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `allacocountingnotbooks`
--

CREATE TABLE `allacocountingnotbooks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `myaccountants_id` bigint(20) UNSIGNED NOT NULL,
  `accounting_notebooks_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `allacocountingnotbooks`
--

INSERT INTO `allacocountingnotbooks` (`id`, `myaccountants_id`, `accounting_notebooks_id`, `created_at`, `updated_at`) VALUES
(23, 1, 23, '2023-12-25 14:59:59', '2023-12-25 14:59:59'),
(24, 1, 24, '2023-12-25 15:00:59', '2023-12-25 15:00:59'),
(25, 1, 25, '2023-12-25 15:01:15', '2023-12-25 15:01:15'),
(26, 1, 26, '2023-12-25 15:04:36', '2023-12-25 15:04:36'),
(27, 1, 27, '2023-12-25 17:15:22', '2023-12-25 17:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Preston Graham', '2023-12-25 09:20:28', '2023-12-25 09:20:28'),
(2, 'Jennifer Delaney', '2023-12-25 09:20:53', '2023-12-25 09:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `ref_num` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `phone`, `location`, `ref_num`, `details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jared Graham', 'favac@mailinator.com', '+1 (486) 113-7728', 'Hic natus in culpa', '+1 (887) 985-9145', 'Eius dolor soluta ex', '2023-12-25 09:20:05', '2023-12-25 09:20:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `invoice_Date` date DEFAULT NULL,
  `Due_date` date DEFAULT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `price_total` varchar(11) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `Value_Status` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `Payment_Date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_number`, `invoice_Date`, `Due_date`, `supplier_name`, `price_total`, `Status`, `Value_Status`, `note`, `Payment_Date`, `created_at`, `updated_at`) VALUES
(1, '6545', '1996-04-04', '2000-11-01', 'Alden Morrison', '490', 'غير مدفوعة', 2, 'Impedit hic aliquip', NULL, '2023-12-25 07:51:25', '2023-12-25 17:11:21'),
(2, '975', '2023-12-11', '2000-05-26', 'Ciaran Holden', '411', 'مدفوعة', 1, 'Sit non est itaque', '2023-12-24', '2023-12-25 07:51:46', '2023-12-25 07:55:25'),
(3, '384', '2023-12-19', '2011-07-17', 'Sigourney Atkins', '988', 'غير مدفوعة', 2, 'Est sed quibusdam v', NULL, '2023-12-25 07:52:04', '2023-12-25 07:52:04'),
(7, '252', '2023-12-20', '2003-05-25', 'Fritz Mays', '979', 'مدفوعة', 1, 'Illo facere et dolor', NULL, '2023-12-25 07:54:03', '2023-12-25 07:54:03'),
(9, '673', '2023-12-18', '1987-02-11', 'Cailin Butler', '270', 'غير مدفوعة', 2, 'Reiciendis pariatur', NULL, '2023-12-25 07:54:31', '2023-12-25 07:54:31'),
(15, 'Chantale Ward', '2022-09-27', '1986-06-25', 'September Perry', '187', 'مدفوعة', 1, 'Inventore dolor nemo', '2023-12-24', '2023-12-25 07:58:26', '2023-12-25 17:07:07'),
(18, '987', '2023-12-14', '2003-09-22', 'Dieter Morrison', '16', 'غير مدفوعة', 2, 'Praesentium nisi rec', NULL, '2023-12-25 09:08:54', '2023-12-25 09:08:54'),
(20, 'Alma Acosta', '1999-08-29', '2016-08-07', 'Aspen Battle', '45', 'غير مدفوعة', 2, 'Nemo sed ut et eiusm', NULL, '2023-12-25 09:17:35', '2023-12-25 17:11:06'),
(21, '646465', '2023-12-07', '2023-12-20', 'Nayda Waters', '847', 'مدفوعة', 1, 'Itaque facilis ut qu', '1983-11-09', '2023-12-25 09:17:51', '2023-12-25 17:10:28'),
(22, 'H-999', '2003-06-20', '1974-09-07', 'Abdul Haney', '982', 'غير مدفوعة', 2, 'Voluptatum assumenda', NULL, '2023-12-25 17:07:39', '2023-12-25 17:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `invoices_detaills`
--

CREATE TABLE `invoices_detaills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_Invoice` bigint(20) UNSIGNED NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `Value_Status` int(11) NOT NULL,
  `Payment_Date` date DEFAULT NULL,
  `note` text DEFAULT NULL,
  `user` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices_detaills`
--

INSERT INTO `invoices_detaills` (`id`, `id_Invoice`, `invoice_number`, `Status`, `Value_Status`, `Payment_Date`, `note`, `user`, `created_at`, `updated_at`) VALUES
(1, 1, '517', 'غير مدفوعة', 2, NULL, 'Beatae iusto tenetur', 'admin', '2023-12-25 07:51:25', '2023-12-25 07:51:25'),
(2, 2, '975', 'غير مدفوعة', 2, NULL, 'Sit non est itaque', 'admin', '2023-12-25 07:51:46', '2023-12-25 07:51:46'),
(3, 3, '384', 'غير مدفوعة', 2, NULL, 'Est sed quibusdam v', 'admin', '2023-12-25 07:52:04', '2023-12-25 07:52:04'),
(4, 7, '252', 'غير مدفوعة', 2, NULL, 'Illo facere et dolor', 'admin', '2023-12-25 07:54:03', '2023-12-25 07:54:03'),
(5, 9, '673', 'غير مدفوعة', 2, NULL, 'Reiciendis pariatur', 'admin', '2023-12-25 07:54:31', '2023-12-25 07:54:31'),
(6, 2, '975', 'مدفوعة', 1, '2023-12-24', 'Sit non est itaque', 'admin', '2023-12-25 07:55:25', '2023-12-25 07:55:25'),
(7, 2, '975', 'مدفوعة', 1, '2023-12-24', 'Sit non est itaque', 'admin', '2023-12-25 07:55:25', '2023-12-25 07:55:25'),
(8, 15, 'E-999', 'غير مدفوعة', 2, NULL, 'Porro inventore prae', 'admin', '2023-12-25 07:58:26', '2023-12-25 07:58:26'),
(9, 15, 'E-999', 'مدفوعة', 1, '2023-12-24', 'Porro inventore prae', 'admin', '2023-12-25 08:00:54', '2023-12-25 08:00:54'),
(10, 18, '987', 'غير مدفوعة', 2, NULL, 'Praesentium nisi rec', 'admin', '2023-12-25 09:08:54', '2023-12-25 09:08:54'),
(11, 20, '787987', 'غير مدفوعة', 2, NULL, 'dasdasdas', 'admin', '2023-12-25 09:17:35', '2023-12-25 09:17:35'),
(12, 21, '646465', 'غير مدفوعة', 2, NULL, 'Itaque facilis ut qu', 'admin', '2023-12-25 09:17:51', '2023-12-25 09:17:51'),
(13, 22, 'H-999', 'غير مدفوعة', 2, NULL, 'Voluptatum assumenda', 'admin', '2023-12-25 17:07:39', '2023-12-25 17:07:39'),
(14, 21, '646465', 'مدفوعة', 1, '1983-11-09', 'Itaque facilis ut qu', 'admin', '2023-12-25 17:10:28', '2023-12-25 17:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_attachments_tables`
--

CREATE TABLE `invoice_attachments_tables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(999) NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `Created_by` varchar(999) NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_attachments_tables`
--

INSERT INTO `invoice_attachments_tables` (`id`, `file_name`, `invoice_number`, `Created_by`, `invoice_id`, `created_at`, `updated_at`) VALUES
(1, 'osama.jpg', 'H-999', 'admin', 22, '2023-12-25 17:07:39', '2023-12-25 17:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `liquidation_statuses`
--

CREATE TABLE `liquidation_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `liquidation_statuses`
--

INSERT INTO `liquidation_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Keith Barry', '2023-12-25 09:20:19', '2023-12-25 09:20:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_09_095112_create_invoices_table', 1),
(6, '2022_09_13_150219_create_invoices_detaills_table', 1),
(7, '2022_09_13_151146_create_invoice_attachments_tables_table', 1),
(8, '2023_11_03_052750_create_companies_table', 1),
(9, '2023_11_04_173740_create_permission_tables', 1),
(10, '2023_11_06_093201_create_boxes_table', 1),
(11, '2023_11_06_093330_create_send_tos_table', 1),
(12, '2023_11_06_093420_create_liquidation_statuses_table', 1),
(13, '2023_11_06_215219_create_accounting_notebooks_table', 1),
(14, '2023_12_16_171036_create_myaccountants_table', 1),
(15, '2023_12_16_172239_create_allacocountingnotbooks_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `myaccountants`
--

CREATE TABLE `myaccountants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `accountant_number` varchar(50) NOT NULL,
  `accountant_name` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `myaccountants`
--

INSERT INTO `myaccountants` (`id`, `accountant_number`, `accountant_name`, `date`, `note`, `created_at`, `updated_at`) VALUES
(1, '40', 'Adrienne Hendricks', '1971-03-03', 'Fugit ad veniam qu', '2023-12-25 09:18:46', '2023-12-25 09:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) NOT NULL,
  `guard_name` varchar(125) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'الفواتير', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(2, 'اضافة مرفق', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(3, 'الفواتير المدفوعة', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(4, 'الفواتير الغير مدفوعة', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(5, 'الدفاتر المحاسبية', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(6, 'التقارير', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(7, 'الصندوق', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(8, 'المرسل اليه', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(9, 'حالة التصفية', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(10, 'المستخدمين', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(11, 'قائمة المستخدمين', 'web', '2023-12-25 07:42:03', '2023-12-25 07:42:03'),
(12, 'صلاحيات المستخدمين', 'web', '2023-12-25 07:42:04', '2023-12-25 07:42:04'),
(13, 'قائمة الشركات', 'web', '2023-12-25 07:42:04', '2023-12-25 07:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) NOT NULL,
  `guard_name` varchar(125) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'owner', 'web', '2023-12-25 07:42:04', '2023-12-25 07:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `send_tos`
--

CREATE TABLE `send_tos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `send_tos`
--

INSERT INTO `send_tos` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ferris Barrera', '2023-12-25 09:20:25', '2023-12-25 09:20:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `roles_name` text NOT NULL,
  `Status` varchar(10) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `roles_name`, `Status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$12$meOxGtsh0dIAnCylGBwL7OKPGF/7QvN1upxiA9L8kWN8xYJePwhuy', '[\"owner\"]', 'مفعل', NULL, '2023-12-25 07:42:04', '2023-12-25 07:42:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounting_notebooks`
--
ALTER TABLE `accounting_notebooks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounting_notebooks_companies_id_foreign` (`companies_id`),
  ADD KEY `accounting_notebooks_box_id_foreign` (`Box_id`),
  ADD KEY `accounting_notebooks_sendto_id_foreign` (`SendTo_id`),
  ADD KEY `accounting_notebooks_liquidationstatus_id_foreign` (`LiquidationStatus_id`);

--
-- Indexes for table `allacocountingnotbooks`
--
ALTER TABLE `allacocountingnotbooks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `allacocountingnotbooks_myaccountants_id_foreign` (`myaccountants_id`),
  ADD KEY `allacocountingnotbooks_accounting_notebooks_id_foreign` (`accounting_notebooks_id`);

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_phone_unique` (`phone`),
  ADD UNIQUE KEY `companies_email_unique` (`email`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_invoice_number_unique` (`invoice_number`);

--
-- Indexes for table `invoices_detaills`
--
ALTER TABLE `invoices_detaills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_detaills_id_invoice_foreign` (`id_Invoice`);

--
-- Indexes for table `invoice_attachments_tables`
--
ALTER TABLE `invoice_attachments_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_attachments_tables_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `liquidation_statuses`
--
ALTER TABLE `liquidation_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `myaccountants`
--
ALTER TABLE `myaccountants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `send_tos`
--
ALTER TABLE `send_tos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounting_notebooks`
--
ALTER TABLE `accounting_notebooks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `allacocountingnotbooks`
--
ALTER TABLE `allacocountingnotbooks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `invoices_detaills`
--
ALTER TABLE `invoices_detaills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `invoice_attachments_tables`
--
ALTER TABLE `invoice_attachments_tables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `liquidation_statuses`
--
ALTER TABLE `liquidation_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `myaccountants`
--
ALTER TABLE `myaccountants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `send_tos`
--
ALTER TABLE `send_tos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounting_notebooks`
--
ALTER TABLE `accounting_notebooks`
  ADD CONSTRAINT `accounting_notebooks_box_id_foreign` FOREIGN KEY (`Box_id`) REFERENCES `boxes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `accounting_notebooks_companies_id_foreign` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `accounting_notebooks_liquidationstatus_id_foreign` FOREIGN KEY (`LiquidationStatus_id`) REFERENCES `liquidation_statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `accounting_notebooks_sendto_id_foreign` FOREIGN KEY (`SendTo_id`) REFERENCES `send_tos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `allacocountingnotbooks`
--
ALTER TABLE `allacocountingnotbooks`
  ADD CONSTRAINT `allacocountingnotbooks_accounting_notebooks_id_foreign` FOREIGN KEY (`accounting_notebooks_id`) REFERENCES `accounting_notebooks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `allacocountingnotbooks_myaccountants_id_foreign` FOREIGN KEY (`myaccountants_id`) REFERENCES `myaccountants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices_detaills`
--
ALTER TABLE `invoices_detaills`
  ADD CONSTRAINT `invoices_detaills_id_invoice_foreign` FOREIGN KEY (`id_Invoice`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_attachments_tables`
--
ALTER TABLE `invoice_attachments_tables`
  ADD CONSTRAINT `invoice_attachments_tables_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
