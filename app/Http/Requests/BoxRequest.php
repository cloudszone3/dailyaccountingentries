<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BoxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'  =>  'required|string|min:3|unique:boxes,name,' . $this->id
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>  'من فضلك قم بإدخال اسم الصندوق' ,
            'name.string'   =>  'يجب ان يكون اسم الصندوق عباره عن احرف فقط' ,
            'name.min'      =>  'يجب ان يكون عدد احرف اسم الصندوق اكبر من 3 احرف' ,
            'name.unique'   =>  'يوجد صندوق بهذا الاسم . من فضلك قم بإدخال اسم اخر'
        ] ;
    }
}
