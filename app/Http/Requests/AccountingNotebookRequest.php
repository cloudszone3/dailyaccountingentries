<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountingNotebookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'Constraint'            =>  'required' ,
            'InvoiceNumber'         =>  'sometimes|nullable' ,
            'display_number'         => 'sometimes|nullable' ,
            'DateOfRegistration'    =>  'required|date',
            // 'Creditor'              =>  'numeric|sometimes|nullable',
            // 'Debtor'                =>  'numeric|sometimes|nullable',
            'Creditor'              =>  'required_without:Debtor',
            'Debtor'                =>  'required_without:Creditor',
            'Box_id'                =>  'required',
            'SendTo_id'             =>  'required',
            'LiquidationStatus_id'  =>  'required',
            'ReferenceNumber'       =>  'sometimes|nullable',
            'photo'                 =>  'sometimes',
        ];
    }


    public function messages()
    {
        return [
            'Constraint.required'           =>  'من فضلك قم بإدخال مبلغ القيد' ,
            // 'InvoiceNumber.required'        =>  'من فضلك قم بإدخال رقم الفاتوره' ,
            // 'display_number.required'        =>  'من فضلك قم بإدخال رقم العرض' ,
            'DateOfRegistration.required'   =>  'من فضلك قم بإدخال التاريخ' ,
            'DateOfRegistration.date'       =>  'قم بكتابه التاريخ بشكل صحيح' ,
            'Creditor.numeric'              =>  'قم بإدخال رقم وليس نص' ,
            'Debtor.numeric'                =>  'قم بإدخال رقم وليس نص' ,
            'Box_id.required'               =>  'من فضلك قم بتحديد الصندوق' ,
            'SendTo_id.required'            =>  'من فضلك قم بتحديد المرسل اليه' ,
            'LiquidationStatus_id.required' =>  'من فضلك قم بتحديد حاله التصفيه' ,
            'photo.mimes'         =>  'للأسف لا نقبل الى الصيغ التاليه [pdf, jpeg ,.jpg , png]' ,
        ] ;
    }
}
