<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiquidationStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'  =>  'required|string|min:3|unique:liquidation_statuses,name,' . $this->id
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>  'من فضلك قم بإدخال اسم حاله التصفيه' ,
            'name.string'   =>  'يجب ان يكون اسم حاله التصفيه عباره عن احرف فقط' ,
            'name.min'      =>  'يجب ان يكون عدد احرف اسم حاله التصفيه اكبر من 3 احرف' ,
            'name.unique'   =>  'يوجد حاله تصفيه بهذا الاسم . من فضلك قم بإدخال اسم اخر'
        ] ;
    }
}
