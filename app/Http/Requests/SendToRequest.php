<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendToRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'  =>  'required|string|min:3|unique:send_tos,name,' . $this->id
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>  'من فضلك قم بإدخال اسم المرسل' ,
            'name.string'   =>  'يجب ان يكون اسم المرسل عباره عن احرف فقط' ,
            'name.min'      =>  'يجب ان يكون عدد احرف اسم المرسل اكبر من 3 احرف' ,
            'name.unique'   =>  'يوجد مرسل اليه بهذا الاسم . من فضلك قم بإدخال اسم اخر'
        ] ;
    }
}
