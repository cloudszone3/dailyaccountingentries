<?php

namespace App\Http\Controllers;

use App\Models\invoices;
use PDF;
use Illuminate\Http\Request;

class Invoices_Report extends Controller
{

    public function index()
    {
        return view('reports.invoices_report');
    }

    public function Search_invoices(Request $request)
    {
        $rdio = $request->rdio;
        // في حالة البحث بنوع الفاتورة
        if ($rdio == 1) {
            // في حالة عدم تحديد تاريخ
            if ($request->type && $request->start_at == '' && $request->end_at == '') {

                $invoices = invoices::select('*')->where('Status', '=', $request->type)->get();
                $type = $request->type;
                return view('reports.invoices_report', compact('type'))->withDetails($invoices);
            }

            // في حالة تحديد تاريخ استحقاق
            else {

                $start_at = date($request->start_at);
                $end_at = date($request->end_at);
                $type = $request->type;

                $invoices = invoices::whereBetween('invoice_Date', [$start_at, $end_at])->where('Status', '=', $request->type)->get();
                return view('reports.invoices_report', compact('type', 'start_at', 'end_at'))->withDetails($invoices);
            }
        }
        //====================================================================

        // في البحث برقم الفاتورة
        else {
            $invoices = invoices::select('*')->where('invoice_number', '=', $request->invoice_number)->get();
            return view('reports.invoices_report')->withDetails($invoices);
        }
    }

    public function export_report_pdf(Request $request)
    {
        if ($request->type == 1) {
            $customPaper = array(0,0,667.00,1500.90);
            $invoices = invoices::query()->where(['Value_Status' => 1])->get()->toArray();
            $pdf = PDF::loadView('pdf.paid_invoice', ['invoices' => $invoices])->setPaper($customPaper, 'landscape');
            return $pdf->download('Paid_Invoices.pdf');
        } else {
            $customPaper = array(0,0,667.00,1400.90);
            $invoices = invoices::query()->where(['Value_Status' => 2])->get()->toArray();
            $pdf = PDF::loadView('pdf.unpaid_invoice', ['invoices' => $invoices])->setPaper($customPaper, 'landscape');
            return $pdf->download('UnPaid_Invoices.pdf');
        }
    }

    public function paid_invoices()
    {
        $invoices = invoices::query()->where(['Value_Status' => 1])->get();
        return view('pdf.paid_invoice', compact('invoices'));
    }

    public function unpaid_invoices()
    {
        $invoices = invoices::query()->where(['Value_Status' => 2])->get();
        return view('pdf.unpaid_invoice', compact('invoices'));
    }
}
