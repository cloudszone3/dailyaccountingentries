<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendToRequest;
use App\Models\SendTo;
use Illuminate\Http\Request;

class SendToController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:المرسل اليه', ['only' => ['index']]);
        // $this->middleware('can:اضافة مرسل الية', ['only' => ['create', 'store']]);
        // $this->middleware('can:تعديل مرسل اليه', ['only' => ['edit', 'update']]);
        // $this->middleware('can:حذف مرسل اليه', ['only' => ['destroy']]);
    }

    public function index()
    {
        $Addressees = SendTo::all();
        return view('AccountingNotebook.sendTo.index')->with('Addressees', $Addressees);
    }

    public function create()
    {
        return view('AccountingNotebook.sendTo.create');
    }

    public function store(SendToRequest $request)
    {
        try {

            SendTo::create([
                'name'  =>  $request->name
            ]);

            return response()->json([
                'status'  => true,
                'msg'     => 'تم الحفظ بنجاح',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
    }

    public function edit($id)
    {
        try {

            if (isset($id) && is_numeric($id)) {

                $sendTo = SendTo::find($id);

                if ($sendTo) {
                    return view('AccountingNotebook.sendTo.edit')->with('sendTo', $sendTo);
                }
            }

            return redirect()->route('AccountingNotebook.SendTo.index');
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.SendTo.index');
        }
    }

    public function update(SendToRequest $request)
    {
        try {

            $sendTo = SendTo::find($request->id);

            if (isset($sendTo) && $sendTo->count() > 0) {

                $sendTo->update([
                    'name'  =>  $request->name
                ]);

                return response()->json([
                    'status'  => true,
                ]);
            } else {
                return response()->json([
                    'status'    =>  false,
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $SendTo = SendTo::find($request->id);
            if ($SendTo) {

                $SendTo->delete();

                session()->flash('delete_SendTo');
                return redirect()->route('AccountingNotebook.SendTo.index');
            }
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.SendTo.index');
        }
    }
}
