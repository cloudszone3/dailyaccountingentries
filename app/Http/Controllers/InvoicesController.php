<?php

namespace App\Http\Controllers;

use Proengsoft\JsValidation\Facades\JsValidatorFacade;
use App\Models\invoices;
use App\Models\invoices_detaills;
use App\Models\invoice_attachments_table;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Exports\InvoicesExport;



class InvoicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:الفواتير', ['only' => ['index']]);
        // $this->middleware('can:اضافة فاتورة', ['only' => ['create', 'store']]);
        // $this->middleware('can:تعديل الفاتورة', ['only' => ['edit', 'update']]);
        // $this->middleware('can:حذف الفاتورة', ['only' => ['destroy']]);
        // $this->middleware('can:تغير حالة الدفع', ['only' => ['Status_Update']]);
        // $this->middleware('can:تغير حالة الدفع', ['only' => ['Invoice_Paid']]);
        // $this->middleware('can:تغير حالة الدفع', ['only' => ['Invoice_unPaid']]);
        // $this->middleware('can:طباعةالفاتورة', ['only' => ['Print_invoice']]);
        // $this->middleware('can:تصدير EXCEL', ['only' => ['export']]);
    }

    protected $validationRules = [
        'invoice_number' => 'required',
        'invoice_Date' => 'required',
        'Due_date' => 'required',
        'supplier_name' => 'required',
        'price_total' => 'required',
        'Status' => 'required',
        'Value_Status' => 'required',
        // 'note' => 'required'
    ];

    public function index()
    {
         $invoices = invoices::with(['getfile'])->get();

        return view('invoices.invoices', compact('invoices'));
    }

    public function create()
    {
        //  $sections = sections::all();
        $validator = JsValidatorFacade::make($this->validationRules);
        return view('invoices.add_invoice', compact('validator'));
    }

    public function store(Request $request)
    {
        try {
        invoices::create([
            'invoice_number' => $request->invoice_number,
            'invoice_Date' => $request->invoice_Date,
            'Due_date' => $request->Due_date,
            'supplier_name' => $request->supplier_name,
            'price_total' => $request->price_total,
            'Status' => 'غير مدفوعة',
            'Value_Status' => 2,
            'note' => $request->note,
        ]);
        $invoice_id = invoices::latest()->first()->id;
        invoices_detaills::create([
            'id_Invoice' => $invoice_id,
            'invoice_number' => $request->invoice_number,
            'supplier_name' => $request->supplier_name,
            'Status' => 'غير مدفوعة',
            'Value_Status' => 2,
            'note' => $request->note,
            'user' => (Auth::user()->name),
        ]);
        if ($request->hasFile('pic')) {

            $invoice_id = Invoices::latest()->first()->id;
            $image = $request->file('pic');
            $file_name = $image->getClientOriginalName();
            $invoice_number = $request->invoice_number;

            $attachments = new invoice_attachments_table();
            $attachments->file_name = $file_name;
            $attachments->invoice_number = $invoice_number;
            $attachments->Created_by = Auth::user()->name;
            $attachments->invoice_id = $invoice_id;
            $attachments->save();

            // move pic
            $imageName = $request->pic->getClientOriginalName();
            $request->pic->move(public_path('Attachments/' . $invoice_number), $imageName);
        }

        return redirect()->route('invoices.index')->with('success', 'تم اضافة الفاتورة بنجاح');
    } catch (\Throwable $th) {
        // Redirect with error message
        return redirect()->route('invoices.index')->with(['error' => 'قمت بالاضافة بشكل خاطىء!']);
    }
    }

    public function show($id)
    {
        $invoices = invoices::where('id', $id)->first();
        return view('invoices.status_update', compact('invoices'));
    }

    public function edit($id)
    {
        $invoices = invoices::where('id', $id)->first();
        $validator = JsValidatorFacade::make($this->validationRules);
        return view('invoices.edit_invoice', compact('invoices', 'validator'));
    }

    public function update(Request $request)
    {
        $invoices = invoices::findOrFail($request->invoice_id);
        $invoices->update([
            'invoice_number' => $request->invoice_number,
            'invoice_Date' => $request->invoice_Date,
            'Due_date' => $request->Due_date,
            'supplier_name' => $request->supplier_name,
            'price_total' => $request->price_total,
            'note' => $request->note,
        ]);

        return redirect()->route('invoices.index')->with('edit', 'تم تعديل الفاتورة بنجاح');
    }

    public function destroy(Request $request)
    {
        $id = $request->invoice_id;
        $invoices = invoices::where('id', $id)->first();
        $Details = invoice_attachments_table::where('invoice_id', $id)->first();

        $id_page = $request->id_page;


        if (!$id_page == 2) {

            if (!empty($Details->invoice_number)) {

                Storage::disk('public_uploads')->deleteDirectory($Details->invoice_number);
            }

            $invoices->forceDelete();
            // $invoices->Delete();
            session()->flash('delete_invoice');
            return redirect('/invoices');
        } else {

            $invoices->delete();
            session()->flash('archive_invoice');
            return redirect('/Archive');
        }
    }

    public function Status_Update($id, Request $request)
    {
        $invoices = invoices::findOrFail($id);

        if ($request->Status === 'مدفوعة') {

            $invoices->update([
                'Value_Status' => 1,
                'Status' => $request->Status,
                'Payment_Date' => $request->Payment_Date,
            ]);

            invoices_Detaills::create([
                'id_Invoice' => $request->invoice_id,
                'invoice_number' => $request->invoice_number,
                'Status' => $request->Status,
                'Value_Status' => 1,
                'note' => $request->note,
                'Payment_Date' => $request->Payment_Date,
                'user' => (Auth::user()->name),
            ]);
        } else {
            $invoices->update([
                'Value_Status' => 3,
                'Status' => $request->Status,
                'Payment_Date' => $request->Payment_Date,
            ]);
            invoices_Detaills::create([
                'id_Invoice' => $request->invoice_id,
                'invoice_number' => $request->invoice_number,
                'Status' => $request->Status,
                'Value_Status' => 3,
                'note' => $request->note,
                'Payment_Date' => $request->Payment_Date,
                'user' => (Auth::user()->name),
            ]);
        }
        session()->flash('Status_Update');
        return redirect('/invoices');
    }
    public function Invoice_Paid()
    {
        $invoices = Invoices::where('Value_Status', 1)->get();
        return view('invoices.invoices_paid', compact('invoices'));
    }

    public function Invoice_unPaid()
    {
        $invoices = Invoices::where('Value_Status', 2)->get();
        return view('invoices.invoices_unpaid', compact('invoices'));
    }


    public function Print_invoice($id)
    {
        $invoices = invoices::where('id', $id)->first();
        return view('invoices.Print_invoice', compact('invoices'));
    }

    public function export()
    {
        return Excel::download(new InvoicesExport, 'invoices.xlsx');
    }
}
