<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Company;
use Illuminate\Http\Request;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:قائمة الشركات', ['only' => ['index']]);
        // $this->middleware('can:اضافة شركة', ['only' => ['create', 'store']]);
        // $this->middleware('can:تعديل شركة', ['only' => ['edit', 'update']]);
        // $this->middleware('can:حذف شركة', ['only' => ['destroy']]);
    }

    protected $validationRules = [
        'name' => 'required',
        'email' => 'required',
        'details' => 'required',
        'phone' => 'required',
        'ref_num' => 'required',
        'location' => 'required',
    ];

    public function index()
    {
        $companyes = Company::all();
        return view('users.show_company', compact('companyes'));
    }

    public function create()
    {
        $validator = JsValidatorFacade::make($this->validationRules);
        return view('users.addcompany', compact('validator'));
    }

    public function store(Request $request){
        try {
        Company::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'details' => $request['details'],
            'phone' => $request['phone'],
            'ref_num' => $request['ref_num'],
            'location' => $request['location'],
        ]);
        return redirect()->route('company.allcompany')->with('success', 'تمت أضافة الشركة بنجاح');
         } catch (\Throwable $th) {
            // Redirect with error message
            return redirect()->route('company.allcompany')->with(['error' => 'such as error!']);
        }
    }

    public function edit($id)
    {
        $company = Company::find($id);
        return view('users.edit_company', compact('company'));
    }

    public function update(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'details' => 'required',
            'ref_num' => 'required',
            'location' => 'required',
        ]);
        $company = Company::where('id', '=', $request->id)->first();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->phone = $request->phone;
        $company->details = $request->details;
        $company->ref_num = $request->ref_num;
        $company->location = $request->location;
        $company->save();
        if ($company) {
            return redirect()->route('company.allcompany')->with('success', 'تم تعديل الشركة بنجاح');
        } else {
            return redirect()->route('company.allcompany')->with(['error' => 'لم يتم تعديل الشركة بنجاح']);
        }
    }

    public function destroy($id){
        $company = Company::where(['id' => $id])->delete();
        if ($company) {
            return redirect()->route('company.allcompany')->with('success', 'تم حذف الشركة بنجاح');
        }else {
            return redirect()->route('company.allcompany')->with(['error' => 'لم يتم حذف الشركة بنجاح']);
    }
}

}
