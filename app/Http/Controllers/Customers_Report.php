<?php

namespace App\Http\Controllers;

use App\Models\invoices;
use Illuminate\Http\Request;

class Customers_Report extends Controller
{
    public function __construct()
    {
        $this->middleware('can:التقارير', ['only' => ['Search_customers']]);
        // $this->middleware('can:تقرير الفواتير', ['only' => ['Search_customers']]);
    }

    public function Search_customers(Request $request)
    {

        // في حالة البحث بدون التاريخ

        if ($request->product && $request->start_at == '' && $request->end_at == '') {

            $invoices = invoices::select('*')->where('section_id', '=', $request->Section)->where('product', '=', $request->product)->get();
            return view('reports.customers_report', compact('sections'))->withDetails($invoices);
        }
        // في حالة البحث بتاريخ
        else {
            $start_at = date($request->start_at);
            $end_at = date($request->end_at);

            $invoices = invoices::whereBetween('invoice_Date', [$start_at, $end_at])->where('product', '=', $request->product)->get();
            return view('reports.customers_report', compact('sections'))->withDetails($invoices);
        }
    }
}
