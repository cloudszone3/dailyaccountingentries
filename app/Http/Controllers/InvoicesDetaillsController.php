<?php

namespace App\Http\Controllers;

use App\Models\invoices_detaills;
use App\Models\invoices;
use App\Models\invoice_attachments_table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InvoicesDetaillsController extends Controller
{

    public function edit($id)
    {
        $invoices = invoices::where('id', $id)->first();
        $details  = invoices_Detaills::where('id_Invoice', $id)->get();
        $attachments  = invoice_attachments_table::where('invoice_id', $id)->get();

        return view('invoices.details_invoice', compact('invoices', 'details', 'attachments'));
    }

    public function destroy(Request $request)
    {
        $invoices = invoice_attachments_table::findOrFail($request->id_file);
        $invoices->delete();
        Storage::disk('public_uploads')->delete($request->invoice_number . '/' . $request->file_name);
        session()->flash('delete', 'تم حذف المرفق بنجاح');
        return back();
    }

    // public function get_file($invoice_number, $file_name)
    // {
    //     $contents = Storage::disk('public_uploads')->getDriver()->getAdapter()
    //         ->applyPathPrefix($invoice_number . '/' . $file_name);
    //     return response()->download($contents);
    // }

    public function get_file($invoice_number, $file_name)
{
    $filePath = $invoice_number . '/' . $file_name;
    $disk = Storage::disk('public_uploads');

    try {
        $fullPath = $disk->path($filePath);

        return response()->download($fullPath, $file_name);
    } catch (\Exception $e) {
        // يمكنك إدراج معالجة الأخطاء هنا
        return response()->download($disk);
    }
}

    // public function open_file($invoice_number,$file_name)
    // {
    //     $files = Storage::disk('public_uploads')->getDriver()->getAdapter()
    //     ->applyPathPrefix($invoice_number.'/'.$file_name);
    //     return response()->file($files);
    // }
    public function open_file($invoice_number, $file_name)
    {
        $filePath = $invoice_number . '/' . $file_name;
        $disk = Storage::disk('public_uploads');

        try {
            $fullPath = $disk->path($filePath);

            return response()->file($fullPath);
        } catch (\Exception $e) {
            return response()->file($disk);
        }
    }
}
