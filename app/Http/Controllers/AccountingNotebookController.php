<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountingNotebookRequest;
use App\Models\AccountingNotebook;
use App\Models\Allacocountingnotbook;
use App\Models\box;
use App\Models\Company;
use App\Models\LiquidationStatus;
use App\Models\SendTo;
use App\Traits\imageTrait;
use Illuminate\Http\Request;

class AccountingNotebookController extends Controller
{
    use imageTrait ;  // trait إستدعاء
     public function __construct()
     {
         $this->middleware('can:الدفاتر المحاسبية', ['only' => ['index']]);
        //  $this->middleware('can:اضافة دفتر محاسبي', ['only' => ['create','store']]);
        //  $this->middleware('can:تعديل دفتر المحاسبي', ['only' => ['edit','update']]);
        //  $this->middleware('can:حذف الدفتر المحاسبي', ['only' => ['destroy']]);
     }



    public function index()
    {
        $AccountingNotebooks = AccountingNotebook::all() ;
        return view('AccountingNotebook.index')->with('AccountingNotebooks' , $AccountingNotebooks) ;
    }

    public function create()
    {
        $boxs = box::all() ;
        $Addressees = SendTo::all() ;
        $LiquidationCases = LiquidationStatus::all() ;
        $companyes = Company::all();
        return view('AccountingNotebook.create')->with('boxs' , $boxs)
        ->with('Addressees' , $Addressees)
        ->with('LiquidationCases' , $LiquidationCases)
        ->with('companyes' , $companyes);
    }

    public function store(AccountingNotebookRequest $request)
    {
        if(isset($request->photo) && !empty($request->photo))   {
            $imageName = $this->savePhoto($request->photo , 'AccountingNotebookImages/') ;
        }   else    {
            $imageName = null ;
        }
        if(!is_null($request->Debtor) && !is_null($request->Creditor)) {
            return back()->with(['error' => 'Error in input']);
        }
        $acc = AccountingNotebook::create ([
            'Constraint'  =>  $request->Constraint ,
            'InvoiceNumber' =>  $request->InvoiceNumber ?? '',
            'display_number' =>  $request->display_number ?? '',
            'DateOfRegistration'    =>  $request->DateOfRegistration ,
            'Creditor'              =>  $request->Creditor ?? 0,
            'Debtor'                =>  $request->Debtor ?? 0,
            'companies_id'          =>  $request->companies_id ,
            'Box_id'                =>  $request->Box_id ,
            'SendTo_id'             =>  $request->SendTo_id ,
            'LiquidationStatus_id'  =>  $request->LiquidationStatus_id ,
            'ReferenceNumber'       =>  $request->ReferenceNumber ?? '' ,
            'photo'                 =>  $imageName
        ]) ;

        Allacocountingnotbook::create([
            'myaccountants_id' => $request->myaccountants_id,
            'accounting_notebooks_id' => $acc->id
        ]);
        return response()->json([
            'status'  => true ,
            'msg'     => 'تم الحفظ بنجاح' ,
        ]) ;
    }
    public function edit($id)
    {
        try {

            if ( isset ($id) && is_numeric($id) ) {

                $AccountingNotebook = AccountingNotebook::find($id) ;

                if ($AccountingNotebook)   {
                    $boxs = box::all() ;
                    $Addressees = SendTo::all() ;
                    $LiquidationCases = LiquidationStatus::all() ;
                    $companyes = Company::all();
                    return view('AccountingNotebook.edit')
                    ->with('AccountingNotebook' , $AccountingNotebook)
                    ->with('boxs' , $boxs)->with('Addressees' , $Addressees)
                    ->with('LiquidationCases' , $LiquidationCases)
                    ->with('companyes' , $companyes);
                }
            }

            return redirect()->route('allaccountingnotebook.show') ;

        } catch (\Exception $ex) {
            return redirect()->route('allaccountingnotebook.show') ;
        }
    }

    public function update(AccountingNotebookRequest $request)
    {
        try {

            if(isset($request->photo) && !empty($request->photo))   {
                $imageName = $this->savePhoto($request->photo , 'AccountingNotebookImages/') ;
            }   else    {
                $imageName = null ;
            }

            if(!is_null($request->Debtor) && !is_null($request->Creditor)) {
                return back()->with(['error' => 'Error in input']);
            }

            $AccountingNotebook = AccountingNotebook::find($request->id) ;

            if(isset($AccountingNotebook) && $AccountingNotebook->count() > 0 ) {

                $AccountingNotebook->update ([
                    'Constraint'  =>  $request->Constraint ,
                    'InvoiceNumber' =>  $request->InvoiceNumber ?? '',
                    'display_number' =>  $request->display_number ?? '',
                    'DateOfRegistration'    =>  $request->DateOfRegistration ,
                    'Creditor'              =>  $request->Creditor ?? 0,
                    'Debtor'                =>  $request->Debtor ?? 0,
                    'companies_id'          =>  $request->companies_id ,
                    'Box_id'                =>  $request->Box_id ,
                    'SendTo_id'             =>  $request->SendTo_id ,
                    'LiquidationStatus_id'  =>  $request->LiquidationStatus_id ,
                    'ReferenceNumber'       =>  $request->ReferenceNumber ?? '' ,
                    'photo'                 =>  $imageName
                ]) ;

                return response()->json([
                    'status'  => true ,
                ]) ;

            }   else    {
                return response()->json([
                    'status'    =>  false ,
                ]) ;
            }

        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false ,
            ]) ;
        }
    }

    public function destroy(Request $request)
    {
        try {

            $AccountingNotebook = AccountingNotebook::find($request->id) ;
            if($AccountingNotebook) {
                $AccountingNotebook->delete() ;

                session()->flash('delete_AccountingNotebook');
                return back();
            }

        } catch (\Exception $ex) {
            return back();
        }

    }

    public function search_in_accounting(Request $request) {
        if(!empty($request->search)) {
            $index = 0;
            $accountings = new \StdClass();
            $accouting_ids = array();
            $accountingNotebook = AccountingNotebook::query()->get();
            foreach($accountingNotebook as $key => $acc) {
                if(date('Y-m-d', strtotime($request->search)) == date('Y-m-d', strtotime($acc->DateOfRegistration))) {
                    $accouting_ids[$key] = $acc->id;
                }
            }
            foreach($accouting_ids as $key => $acc_id) {
                $accountings->{$index} = AccountingNotebook::query()->where(['id' => $acc_id])->first();
                $index++;
            }
            return view('AccountingNotebook.data_search', ['AccountingNotebooks' => $accountings]);
        } else {
            return 0;
        }
    }
}
