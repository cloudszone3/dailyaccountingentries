<?php

namespace App\Http\Controllers;

use App\Models\Allacocountingnotbook;
use App\Models\box;
use App\Models\Company;
use App\Models\LiquidationStatus;
use App\Models\Myaccountant;
use App\Models\SendTo;
use Illuminate\Http\Request;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;


class MyAccountantsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:الدفاتر المحاسبية', ['only' => ['index']]);
    }

    protected $validationRules = [
        'accountant_number' => 'required',
        'accountant_name' => 'required',
        'date' => 'required',
        // 'note' => 'required',
    ];

    public function index(){
        $validator = JsValidatorFacade::make($this->validationRules);
        $myaccountants = Myaccountant::all() ;
        return view('MyAccountants.index' , compact('myaccountants','validator'));
    }

    public function show($id){
         $allacocountingnotbooks = Allacocountingnotbook::with(['accountting'])->where(['myaccountants_id' => $id])->get();
         $myaccountants = Myaccountant::all() ;
         $dftr_id = $id;
         return view('MyAccountants.show',compact('allacocountingnotbooks','myaccountants','dftr_id'));
    }

    public function create($id){
        $boxs = box::all() ;
        $Addressees = SendTo::all() ;
        $LiquidationCases = LiquidationStatus::all() ;
        $companyes = Company::all();
        $validator = JsValidatorFacade::make($this->validationRules);
        $myaccountants = Myaccountant::all() ;
        $dftr_id = $id;
        return view('MyAccountants.create' ,
         compact('myaccountants','validator','companyes','LiquidationCases','boxs','Addressees', 'dftr_id'));
    }

    public function store(Request $request){
        Myaccountant::create([
            'accountant_number' => $request['accountant_number'],
            'accountant_name' => $request['accountant_name'],
            'date' => $request['date'],
            'note' => $request['note'],
        ]);
        return redirect()->route('myaccountants')->with('success', 'تم إنشاء دفتر بنجاح');
    }

    public function edit($id){
        $validator = JsValidatorFacade::make($this->validationRules);
        $myaccountant = Myaccountant::find($id);
        return view('MyAccountants.edit', compact('myaccountant','validator'));
    }

    public function update(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'accountant_number' => 'required',
            'accountant_name' => 'required',
            'date' => 'required',
            'note' => 'sometimes',
        ]);
        $myaccountant = Myaccountant::where('id', '=', $request->id)->first();
        $myaccountant->accountant_number = $request->accountant_number;
        $myaccountant->accountant_name = $request->accountant_name;
        $myaccountant->date = $request->date;
        $myaccountant->note = $request->note;
        $myaccountant->save();
        if ($myaccountant) {
            return redirect()->route('myaccountants')->with('success', 'تم تعديل دفتر بنجاح');
        } else{
            return redirect()->route('myaccountants')->with('error', 'لم يتم تعديل دفتر');
        }
}

    public function destroy($id){
        $myaccountant = Myaccountant::where(['id' => $id])->delete();
        if ($myaccountant) {
            return redirect()->route('myaccountants')->with('success', 'تم حذف دفتر بنجاح');
        } else {
            return redirect()->route('myaccountants')->with('error', 'لم يتم حذف دفتر');
        }
    }

    public function editqayd($id){
        $validator = JsValidatorFacade::make($this->validationRules);
        $myaccountant = Myaccountant::find($id);
        return view('MyAccountants.edit', compact('myaccountant','validator'));
    }

}
