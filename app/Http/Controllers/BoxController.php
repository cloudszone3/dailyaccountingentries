<?php

namespace App\Http\Controllers;
use App\Http\Requests\BoxRequest;
use App\Models\box;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:الصندوق', ['only' => ['index']]);
        // $this->middleware('can:اضافة صندوق', ['only' => ['create', 'store']]);
        // $this->middleware('can:تعديل صندوق', ['only' => ['edit', 'update']]);
        // $this->middleware('can:حذف صندوق', ['only' => ['destroy']]);
    }

    public function index()
    {
        $boxs = box::all();
        return view('AccountingNotebook.boxs.index')->with('boxs', $boxs);
    }

    public function create()
    {
        return view('AccountingNotebook.boxs.create');
    }

    public function store(BoxRequest $request)
    {
        try {

            box::create([
                'name'  =>  $request->name
            ]);

            return response()->json([
                'status'  => true,
                'msg'     => 'تم الحفظ بنجاح',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
    }

    public function edit($id)
    {
        try {

            if (isset($id) && is_numeric($id)) {

                $box = box::find($id);

                if ($box) {
                    return view('AccountingNotebook.boxs.edit')->with('box', $box);
                }
            }

            return redirect()->route('AccountingNotebook.box.index');
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.box.index');
        }
    }

    public function update(BoxRequest $request)
    {
        try {

            $box = box::find($request->id);

            if (isset($box) && $box->count() > 0) {

                $box->update([
                    'name'  =>  $request->name
                ]);

                return response()->json([
                    'status'  => true,
                ]);
            } else {
                return response()->json([
                    'status'    =>  false,
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $box = box::find($request->id);
            if ($box) {

                $box->delete();

                session()->flash('delete_box');
                return redirect()->route('AccountingNotebook.box.index');
            }
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.box.index');
        }
    }
}
