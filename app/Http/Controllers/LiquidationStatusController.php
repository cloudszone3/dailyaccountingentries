<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiquidationStatusRequest;
use App\Models\LiquidationStatus;
use Illuminate\Http\Request;

class LiquidationStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:حالة التصفية', ['only' => ['index']]);

    }

    public function index()
    {
        $LiquidationCases = LiquidationStatus::all();
        return view('AccountingNotebook.LiquidationStatus.index')->with('LiquidationCases', $LiquidationCases);
    }

    public function create()
    {
        return view('AccountingNotebook.LiquidationStatus.create');
    }

    public function store(Request $request)
    {
        try {

            LiquidationStatus::create([
                'name'  =>  $request->name
            ]);


            return response()->json([
                'status'  => true,
                'msg'     => 'تم الحفظ بنجاح',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
        // LiquidationStatus::create($request->all());

        // return redirect()->route('AccountingNotebook.LiquidationStatus.index')->with('success', 'تم اضافة التصفية بنجاح');
    }

    public function edit($id)
    {
        try {

            if (isset($id) && is_numeric($id)) {

                $LiquidationStatus = LiquidationStatus::find($id);

                if ($LiquidationStatus) {
                    return view('AccountingNotebook.LiquidationStatus.edit')->with('LiquidationStatus', $LiquidationStatus);
                }
            }

            return redirect()->route('AccountingNotebook.LiquidationStatus.index');
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.LiquidationStatus.index');
        }
    }

    public function update(LiquidationStatusRequest $request)
    {
        try {

            $LiquidationStatus = LiquidationStatus::find($request->id);

            if (isset($LiquidationStatus) && $LiquidationStatus->count() > 0) {

                $LiquidationStatus->update([
                    'name'  =>  $request->name
                ]);

                return response()->json([
                    'status'  => true,
                ]);
            } else {
                return response()->json([
                    'status'    =>  false,
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status'    =>  false,
            ]);
        }
    }

    public function destroy(Request $request)
    {
        try {

            $LiquidationStatus = LiquidationStatus::find($request->id);
            if ($LiquidationStatus) {

                $LiquidationStatus->delete();

                session()->flash('delete_LiquidationStatus');
                return redirect()->route('AccountingNotebook.LiquidationStatus.index');
            }
        } catch (\Exception $ex) {
            return redirect()->route('AccountingNotebook.LiquidationStatus.index');
        }
    }
}
