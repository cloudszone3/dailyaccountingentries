<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    public $table = 'companies';

    protected $fillable = [
    'name',
    'email',
    'phone',
    'location',
    'ref_num',
    'details',

    ];

    protected $hidden = [
         'updated_at',
         'created_at',
         'deleted_at'
    ];

}
