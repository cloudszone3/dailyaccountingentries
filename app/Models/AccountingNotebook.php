<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountingNotebook extends Model
{
    use HasFactory;

    protected $fillable = [
        'Constraint' ,
        'InvoiceNumber' ,
        'display_number' ,
        'DateOfRegistration' ,
        'Creditor' ,
        'Debtor' ,
        'companies_id' ,
        'Box_id' ,
        'SendTo_id' ,
        'LiquidationStatus_id' ,
        'ReferenceNumber' ,
        'photo'
    ] ;

    public function box()
    {
       return $this->belongsTo(box::class, 'Box_id');
    }

    public function SendTo()
    {
       return $this->belongsTo(SendTo::class, 'SendTo_id');
    }

    public function LiquidationStatus()
    {
       return $this->belongsTo(LiquidationStatus::class, 'LiquidationStatus_id');
    }

    public function company()
    {
       return $this->belongsTo(Company::class, 'companies_id');
    }

    public function accountting()
    {
       return $this->belongsTo(Allacocountingnotbook::class, 'accounting_notebooks_id' ,'id' );
    }
}
