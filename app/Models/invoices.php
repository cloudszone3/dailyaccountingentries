<?php

namespace App\Models;
use App\Models\sections;
use App\Http\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class invoices extends Model
{
  use HasFactory;

    protected $fillable = [
        'invoice_number',
        'invoice_Date',
        'Due_date',
        'supplier_name',
        'price_total',
        'Status',
        'Value_Status',
        'note',
        'Payment_Date',
    ];

    protected $dates = ['deleted_at'];
    public function getfile()
    {
        return  $this->hasOne(invoice_attachments_table::class , 'invoice_id', 'id');
    }
}
