<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Myaccountant extends Model
{
    use HasFactory;

    protected $fillable = [
        'accountant_number',
        'accountant_name',
        'date',
        'note',
    ];

    public function myaccountants()
    {
       return $this->hasMany(Allacocountingnotbook::class , 'myaccountants_id' ,'id');
    }
}
