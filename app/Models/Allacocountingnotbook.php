<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allacocountingnotbook extends Model
{
    use HasFactory;

    protected $fillable = [
        'myaccountants_id',
        'accounting_notebooks_id',
    ] ;

    public function myaccountants()
    {
       return $this->belongsTo(Myaccountant::class, 'myaccountants_id');
    }

    public function accountting()
    {
       return $this->belongsTo(AccountingNotebook::class, 'accounting_notebooks_id');
    }

}
