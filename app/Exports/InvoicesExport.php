<?php

namespace App\Exports;

use App\Models\invoices;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class InvoicesExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = invoices::select('invoice_number','invoice_Date','supplier_name','Due_date',
        'price_total','Status','note','Payment_Date','created_at','updated_at')->get();

        foreach ($data as $key => $d) {
            $data[$key]['Due_date']  = date('Y-m-d H:i:s', strtotime($data[$key]['Due_date']));
        }
        return $data;

    }

    public function map($invoice): array
    {
        return [
            $invoice->invoice_number,
            $invoice->invoice_Date,
            $invoice->supplier_name,
            $invoice->Due_date,
            $invoice->price_total,
            $invoice->Status,
            // $invoice->Value_Status,
            $invoice->note,
            $invoice->Payment_Date,
            $invoice->created_at = date('Y-m-d', strtotime($invoice->created_at)),
            $invoice->updated_at = date('Y-m-d', strtotime($invoice->updated_at)),
        ];
    }

    public function headings(): array
    {
        return [
            'invoice_number','invoice_Date','supplier_name','Due_date',
        'price_total','Status','note','Payment_Date','created_at','updated_at'
        ];
    }



}
