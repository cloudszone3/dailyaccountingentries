<?php
namespace App\Traits ;  // مسار الملف

trait imageTrait {
    public function savePhoto($photo , $folder) {
        // إضافة تصحيح لطباعة نوع المتغير $photo
        error_log('Type of $photo: ' . gettype($photo));
        $photoName = time() . rand(100,900) . '.' . $photo->getClientOriginalExtension() ;
        $photo->move($folder , $photoName) ;
        return $photoName ;
    }

}
