<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    $permissions = [

            'الفواتير',
            'اضافة مرفق',
            'الفواتير المدفوعة',
            'الفواتير الغير مدفوعة',

            'الدفاتر المحاسبية',

            'التقارير',

            'الصندوق',

            'المرسل اليه',

            'حالة التصفية',


            'المستخدمين',
            'قائمة المستخدمين',
            'صلاحيات المستخدمين',
            'قائمة الشركات',

    ];



    foreach ($permissions as $permission) {

    Permission::create(['name' => $permission]);
    }


    }
    }


