<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('accounting_notebooks', function (Blueprint $table) {
            $table->id();
            $table->string('Constraint',50) ;
            $table->string('InvoiceNumber', 50) ;
            $table->string('display_number', 50) ;
            $table->date('DateOfRegistration') ;
            $table->float('Creditor') ;
            $table->float('Debtor') ;
            $table->bigInteger('companies_id')->unsigned() ;
            $table->bigInteger('Box_id')->unsigned() ;
            $table->bigInteger('SendTo_id')->unsigned() ;
            $table->bigInteger('LiquidationStatus_id')->unsigned() ;
            $table->string('ReferenceNumber') ;
            $table->string('photo')->nullable();
            // $table->string('photo');
            $table->timestamps();


            $table->foreign('companies_id')->references('id')->on('companies')->onDelete('cascade') ;

            $table->foreign('Box_id')->references('id')->on('boxes')->onDelete('cascade') ;

            $table->foreign('SendTo_id')->references('id')->on('send_tos')->onDelete('cascade') ;

            $table->foreign('LiquidationStatus_id')->references('id')->on('liquidation_statuses')->onDelete('cascade') ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('accounting_notebooks');
    }
};
