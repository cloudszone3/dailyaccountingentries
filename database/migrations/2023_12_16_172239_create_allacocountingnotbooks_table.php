<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('allacocountingnotbooks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('myaccountants_id')->references('id')->on('myaccountants')->onDelete('cascade');
            $table->foreignId('accounting_notebooks_id')->references('id')->on('accounting_notebooks')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('allacocountingnotbooks');
    }
};
