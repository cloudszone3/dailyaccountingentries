<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html;" charset="utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.rtl.min.css" integrity="sha384-gXt9imSW0VcJVHezoNQsP+TNrjYXoGcrqBZJpry9zJt8PCQjobwmhMGaDHTASo9N" crossorigin="anonymous">
    <style>

        body { font-family: DejaVu Sans; Arial }

        table { width: 100%; border-collapse:unset; margin-top: 20px; }

        td { width: 100%; border: 1px solid #5a3838; padding: 10px; }

        th {width: 100%; text-align: center; border: 1px solid #5a3838; padding: 15px; background-color: #04AA6D; color: white; }

    </style>
</head>

<body style="text-align: right !important; font-family: Arial">
    <img src="assets/img/brand/favicon.png" width="150" height="150" class="mb-5 d-flex justify-content-center align-items-center">

    <p class="mb-5 d-flex justify-content-center align-items-center""><b>العنوان : الفواتير  المدفوعة </b><b>التاريخ {{ date('d/m/Y') }}:</b></p>

    <table border="1" class="table">
        <thead>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th >Due Date</th>
            <th>Supplier Name</th>
            <th>Price Total</th>
            <th>Status</th>
            <th>Note</th>
            <th>Payment Date</th>
        </thead>
        <tbody>
            @foreach ($invoices as $key => $invoice)
            <tr>
                {{-- <td>
                    {{ $invoice['id'] }}
                </td> --}}
                <td>
                    {{ $invoice['invoice_number'] }}
                </td>
                <td>
                    {{ $invoice['invoice_Date'] }}
                </td>
                <td>
                    {{ $invoice['Due_date'] }}
                </td>
                <td>
                    {{ $invoice['supplier_name'] }}
                </td>
                <td>
                    {{ $invoice['price_total'] }}
                </td>
                <td>
                    Paid
                </td>
                <td>
                    {{ $invoice['note'] }}
                </td>
                <td style="width: 95px">
                    {{ $invoice['Payment_Date'] }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
