<thead>
    <tr>
        <th class="wd-10 border-bottom-0">#</th>
        <th class="wd-10 border-bottom-0">القيد</th>
        <th class="wd-10 border-bottom-0">رقم الفاتوره</th>
        <th class="wd-10 border-bottom-0">رقم العرض</th>
        <th class="wd-20 border-bottom-0" style="width: 40px">تاريخ القيد</th>
        <th class="wd-10 border-bottom-0">دائن</th>
        <th class="wd-10 border-bottom-0">مدين</th>
        <th class="wd-10 border-bottom-0">شركات</th>
        <th class="wd-10 border-bottom-0">الصندوق</th>
        <th class="wd-10 border-bottom-0">جهة المرسل لها</th>
        <th class="wd-15 border-bottom-0">حاله التصفيه</th>
        <th class="wd-10 border-bottom-0">الرقم المرجعى</th>
        <th class="wd-10 border-bottom-0" style="width: 50px">العمليات</th>
        <th class="wd-10 border-bottom-0"  style="width: 95px">المرفقات</th>
    </tr>
</thead>
<tbody>

 @if (isset($AccountingNotebooks) && !empty($AccountingNotebooks))
        @foreach ($AccountingNotebooks as $key => $AccountingNotebook)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $AccountingNotebook->Constraint }}</td>
                <td>{{ $AccountingNotebook->InvoiceNumber }}</td>
                <td>{{ $AccountingNotebook->display_number }}</td>
                <td>{{ $AccountingNotebook->DateOfRegistration }}</td>
                <td style="color:rgb(0, 255, 8)" >{{+ $AccountingNotebook->Creditor }}</td>
                <td style="color:rgb(255, 0, 0)" >{{- $AccountingNotebook->Debtor }}</td>
                <td style="color:rgb(25, 0, 255)">{{ $AccountingNotebook->company->name }}</td>
                <td>{{ $AccountingNotebook->box->name }}</td>
                <td>{{ $AccountingNotebook->SendTo->name }}</td>
                <td>{{ $AccountingNotebook->LiquidationStatus->name }}</td>
                <td>{{ $AccountingNotebook->ReferenceNumber }}</td>
                {{-- <td>{{ $AccountingNotebook->photo }}</td> --}}
                <td>
                    <a href="{{ route('AccountingNotebook.edit' , $AccountingNotebook) }}" class="btn btn-sm btn-info" title="تعديل">
                        <i class="las la-pen"></i>
                    </a>
                    <a class="modal-effect btn btn-sm btn-danger" data-effect="effect-scale"
                        data-invoice_id="{{ $AccountingNotebook->id }}" data-boxname="{{ $AccountingNotebook->name }}"
                        data-toggle="modal" data-target="#delete_invoice" href="#modaldemo8" title="حذف">
                        <i class="las la-trash"></i>
                    </a>
                </td>

                <td>
                    @if (!is_null($AccountingNotebook->photo))

                    <a class="btn btn-sm btn-info" href="{{ url()->to('/') }}/AccountingNotebookImages/{{ $AccountingNotebook->photo }}" target="_blank" download>
                        <i class="fas fa-download"></i>
                        &nbsp;
                        تحميل</a>
                    @endif

                    @if (!is_null($AccountingNotebook->photo))

                    <a class="btn btn-outline-success btn-sm"  href="{{ url()->to('/') }}/AccountingNotebookImages/{{ $AccountingNotebook->photo }}" target="_blank">
                        <i class="fas fa-eye"></i>
                        &nbsp;
                        عرض</a>
                    @endif

                </td>

            </tr>
         @endforeach
    @endif
</tbody>
