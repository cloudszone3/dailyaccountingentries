@extends('layouts.master')
@section('css')
<!-- Internal Nice-select css  -->
<link href="{{URL::asset('assets/plugins/jquery-nice-select/css/nice-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
@section('title')
تعديل دفتر محاسبى
@stop


@endsection
@section('page-header')
<!-- breadcrumb -->

@if (isset($AccountingNotebook) && !empty($AccountingNotebook))


    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">الدفتر المحاسبى</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل الدفتر المحاسبى</span>
            </div>
        </div>
    </div>
    <!-- breadcrumb -->
    @endsection
    @section('content')
    <!-- row -->
    <div class="row">


        <div class="col-lg-12 col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right">
                            {{-- <a class="btn btn-primary btn-sm" href="{{ route('AccountingNotebook.index') }}">رجوع</a> --}}
                        </div>
                    </div><br>
                    <form action="javascript: void(0);" class="parsley-style-1" id="selectForm2 AccountingNotebookForm" autocomplete="off"
                     name="selectForm2" enctype="multipart/form-data"
                     onsubmit="AccountingNotebookFormCreate(this)" >
                        @csrf
                        {{-- @method('GET') --}}

                        <div class="">
                            <input type="hidden" name="id" value="{{ $AccountingNotebook->id }}">

                            <div class="row mg-b-20 card-body">
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>القيد: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="Constraint" type="text" value="{{ $AccountingNotebook->Constraint }}">

                                    <span class="text-danger" id="Constraint_error"></span>
                                </div>
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>رقم الفاتوره: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="InvoiceNumber" type="text" value="{{ $AccountingNotebook->InvoiceNumber }}">

                                    <span class="text-danger" id="InvoiceNumber_error"></span>
                                </div>

                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>رقم العرض: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="display_number" type="text" value="{{ $AccountingNotebook->display_number }}">

                                    <span class="text-danger" id="display_number_error"></span>
                                </div>
                                {{-- <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>تاريخ القيد: <span class="tx-danger">*</span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="DateOfRegistration" type="text">

                                    <span class="text-danger" id="DateOfRegistration_error"></span>
                                </div> --}}
                                <div class="col">
                                    <label>تاريخ القيد</label>
                                    <input class="form-control fc-datepicker" name="DateOfRegistration" placeholder="YYYY-MM-DD"
                                        type="text" value="{{ $AccountingNotebook->DateOfRegistration }}" required>

                                    <span class="text-danger" id="DateOfRegistration_error"></span>
                                </div>
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>دائن: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="Creditor" type="text" value="{{ $AccountingNotebook->Creditor }}">

                                    <span class="text-danger" id="Creditor_error"></span>
                                </div>
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>مدين: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="Debtor" type="text" value="{{ $AccountingNotebook->Debtor }}">

                                    <span class="text-danger" id="Debtor_error"></span>
                                </div>

                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label> شركات: <span class="tx-danger"></span></label>
                                    @if (isset($companyes) && $companyes->count() >= 0)
                                        <select id="companies_id" class="js-choice"
                                        name="companies_id" placeholder="Native Select"
                                        data-search="false" data-silent-initial-value-set="true">
                                            @foreach ($companyes as $company)
                                                <option
                                                <?php echo ($company->id == $AccountingNotebook->companies_id ? 'selected' : ''); ?>
                                                 value="{{$company->id}}"  >{{$company->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="text-danger" id="Box_id_error"></span>
                                    @else
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="companies_id">
                                                <option disabled selected>لا يوجد اى بيانات</option>
                                        </select><br>
                                    @endif
                                    <span class="text-danger" id="name_error"></span>
                                </div>

                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label> الصندوق: <span class="tx-danger"></span></label>
                                    @if (isset($boxs) && $boxs->count() >= 0)
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="Box_id">
                                            @foreach ($boxs as $box)
                                                <option <?php echo ($box->id == $AccountingNotebook->Box_id ? 'selected' : ''); ?> value="{{$box->id}}"  >{{$box->name}}</option>
                                            @endforeach

                                        </select>
                                        <span class="text-danger" id="Box_id_error"></span>
                                    @else
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="Box_id">
                                                <option disabled selected>لا يوجد اى بيانات</option>
                                        </select><br>
                                    @endif
                                    <span class="text-danger" id="name_error"></span>
                                </div>
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label> جهه المرسل لها: <span class="tx-danger"></span></label>
                                    @if (isset($Addressees) && $Addressees->count() >= 0)
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="SendTo_id">
                                            @foreach ($Addressees as $SendTo)
                                                <option value="{{$SendTo->id}}" <?php echo ($SendTo->id == $AccountingNotebook->SendTo_id ? 'selected' : ''); ?>>{{$SendTo->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger" id="SendTo_id_error"></span>
                                    @else
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="SendTo_id">
                                                <option disabled selected>لا يوجد اى بيانات</option>
                                        </select><br>
                                    @endif
                                    <span class="text-danger" id="name_error"></span>
                                </div>
                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label> حاله التصفيه: <span class="tx-danger"></span></label>
                                    @if (isset($LiquidationCases) && $LiquidationCases->count() >= 0)
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="LiquidationStatus_id">
                                            @foreach ($LiquidationCases as $LiquidationCase)
                                                <option value="{{$LiquidationCase->id}}" <?php echo ($LiquidationCase->id == $AccountingNotebook->LiquidationStatus_id ? 'selected' : ''); ?>>{{$LiquidationCase->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger" id="LiquidationStatus_id_error"></span>
                                    @else
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="LiquidationStatus_id">
                                                <option disabled selected>لا يوجد اى بيانات</option>
                                        </select><br>
                                    @endif
                                    <span class="text-danger" id="name_error"></span>
                                </div>

                                <div class="parsley-input col-md-6" id="fnWrapper">
                                    <label>الرقم المرجعى: <span class="tx-danger"></span></label>
                                    <input class="form-control form-control-sm mg-b-20"
                                        data-parsley-class-handler="#lnWrapper" name="ReferenceNumber" type="text" value="{{ $AccountingNotebook->ReferenceNumber }}">

                                    <span class="text-danger" id="ReferenceNumber_error"></span>
                                </div>
                                <br>

                                <div style="display: block ; width: 100%;">
                                    <p class="text-danger"> صيغة المرفق pdf, jpeg ,.jpg , png </p>
                                    <h5 class="card-title">المرفقات</h5>

                                    <div >
                                        <img class="avatar mb-2 avatar-lg" src="{{asset('AccountingNotebookImages/' . $AccountingNotebook->photo)}}" alt="">
                                        <input type="file" name="photo" id="photo" name="photo" accept="photo/*"  value="{{ $AccountingNotebook->photo }}"
                                        class="dropify form-control form-control-sm mg-b-20" accept=".pdf,.jpg, .png, image/jpeg, image/png"
                                            data-height="70" style="height: 90" />
                                    </div><br>
                                </div>


                            </div>

                            <div id="msg_AccountingNotebook" style="display: none;"></div>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button class="btn btn-main-primary pd-x-20" type="submit" id="edit_AccountingNotebook">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- row closed -->
    </div>
    <!-- Container closed -->
    </div>
    <!-- main-content closed -->
    @endsection

@endif


@section('js')


<!-- Internal Nice-select js-->
<script src="{{URL::asset('assets/plugins/jquery-nice-select/js/jquery.nice-select.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-nice-select/js/nice-select.js')}}"></script>

<!--Internal  Parsley.min js -->
<script src="{{URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('assets/js/form-validation.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.css">

<script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>

<script>
    const element = document.querySelector('.js-choice');
    const choices = new Choices(element);
</script>

<script>
        function AccountingNotebookFormCreate(element) {
        var form = new FormData(element);
            $.ajax({
                type : 'POST',
                enctype : "multipart/form-data" ,
                url  : "{{ route('AccountingNotebook.update') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form,
                success : function (data) {
                    if (data.status == true) {
                        $('#msg_AccountingNotebook').show();
                        $("#notAdded").hide();
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "{{ 'تم التحديث بنجاح' }}",
                            timeout: 2000,
                            killer: true
                        }).show();
                        // location.href = "{{ url()->to('/') }}/AccountingNotebook/index"
                        // window.history.go(-1);
                        window.location=document.referrer;
                    }   else    {
                        $('#msg_AccountingNotebook').show();
                        $("#notAdded").hide();
                        new Noty({
                            type: 'error',
                            layout: 'topRight',
                            text: "{{ 'حدث خطأ ما . يرجى اعاده المحاوله فى وقت لاحق ' }}",
                            timeout: 2000,
                            killer: true
                        }).show();
                    }
                },
                error : function (errorMessages) {

                    var messages = JSON.parse(errorMessages.responseText);

                    $.each(messages.errors , function (key , val) {

                        key = key.replace(/[.]/g , '_') ;
                        $("#" + key + "_error").text(val);  // It shows product name errors

                    });
                }
            });
    }
    </script>

@endsection
