@extends('layouts.master')
@section('css')
<!-- Internal Nice-select css  -->
<link href="{{URL::asset('assets/plugins/jquery-nice-select/css/nice-select.css')}}" rel="stylesheet" />
@section('title')
تعديل الصندوق
@stop


@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">الصناديق</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل الصندوق</span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
<div class="row">


    <div class="col-lg-12 col-md-12">

        <div class="card">
            <div class="card-body">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary btn-sm" href="{{ route('AccountingNotebook.box.index') }}">رجوع</a>
                    </div>
                </div><br>
                <form class="parsley-style-1" id="selectForm2" autocomplete="off" name="selectForm2" >
                    {{-- @csrf
                    @method('GET') --}}
                    <input type="hidden" name="id" value="{{ $box->id }}">

                    <div class="">

                        <div class="row mg-b-20">
                            <div class="parsley-input col-md-6" id="fnWrapper">
                                <label>اسم الصندوق: <span class="tx-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm mg-b-20"
                                    data-parsley-class-handler="#lnWrapper" name="name"  value="{{ $box->name }}">

                                <span class="text-danger" id="name_error"></span>
                            </div>
                        </div>

                        <div id="msg_box" style="display: none;"></div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button class="btn btn-main-primary pd-x-20" type="submit" id="edit_box">تعديل</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')


<!-- Internal Nice-select js-->
<script src="{{URL::asset('assets/plugins/jquery-nice-select/js/jquery.nice-select.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-nice-select/js/nice-select.js')}}"></script>

<!--Internal  Parsley.min js -->
<script src="{{URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!-- Internal Form-validation js -->
<script src="{{URL::asset('assets/js/form-validation.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.css">


<script>
    $(document).ready(function() {
        $(document).on('click', '#edit_box', function(e) {
            e.preventDefault();
            $("#name_error").text("");
            $.ajax({
                type : 'POST',
                url  : "{{ route('AccountingNotebook.box.update') }}",
                data : {
                    '_token'    : "{{csrf_token()}}",
                    'id'        : $("input[name='id']").val(),
                    'name'      : $("input[name='name']").val(),
                },
                success : function (data) {
                    if (data.status == true) {
                        $('#msg_box').show();
                        $("#notAdded").hide();
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "{{ 'تم تحديث الصندوق بنجاح ' }}",
                            timeout: 2000,
                            killer: true
                        }).show();
                        location.href = "{{ url()->to('/') }}/AccountingNotebook/box"
                    }   else    {
                        $('#msg_box').show();
                        $("#notAdded").hide();
                        new Noty({
                            type: 'error',
                            layout: 'topRight',
                            text: "{{ 'حدث خطأ ما . يرجى اعاده المحاوله فى وقت لاحق ' }}",
                            timeout: 2000,
                            killer: true
                        }).show();
                    }
                },
                error : function (errorMessages) {

                    var messages = JSON.parse(errorMessages.responseText);

                    $.each(messages.errors , function (key , val) {

                        key = key.replace(/[.]/g , '_') ;
                        $("#" + key + "_error").text(val);  // It shows product name errors

                    });
                }
            });
            return false;
        });
    });
    </script>

@endsection
