@extends('layouts.master')
@section('css')
@section('title')
    أضافة شركة
@stop
<!--- Internal Select2 css-->
<link href="{{URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection
@section('page-header')
				<!-- breadcrumb -->
				<div class="breadcrumb-header justify-content-between">
					<div class="my-auto">
						<div class="d-flex">
							<h4 class="content-title mb-0 my-auto"> إضافة شركة</h4>
                        </div>
					</div>
				</div>
				<!-- breadcrumb -->
@endsection
@section('content')
@if (session()->has('Add'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session()->get('Add') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
				<!-- row -->
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                 <form  id="formCompany" method = "POST" action="{{ route('company.addCompany') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}

										<div class="control-group form-group">
											<label class="form-label">اسم الشركة</label>
											<input type="text" class="form-control required" name="name"  >
										</div>

										<div class="control-group form-group">
											<label class="form-label">الإيميل</label>
											<input type="email" class="form-control required" name="email"  >
										</div>

										<div class="control-group form-group">
											<label class="form-label">رقم الهاتف</label>
											<input type="tel" class="form-control required" name="phone"  >
										</div>

                                        <div class="control-group form-group">
											<label class="form-label">العنوان  </label>
											<input type="text" class="form-control required" name="location"  >
										</div>

										<div class="control-group form-group">
											<label class="form-label">رقم مرجعي للشركة </label>
											<input type="tel" class="form-control required" name="ref_num"  >
										</div>

										<div class="control-group form-group">
											<label class="form-label">معلومات اكثر عن شركة  </label>
											<input type="text" class="form-control required" name="details"  >
										</div>

                                    <button style="margin-top: 20px;"  type="submit" class="btn btn-success" >  حفظ البيانات </button>
                                    </form>


							</div>
						</div>
					</div>

				</div>
				<!-- /row -->


			</div>
			<!-- Container closed -->
		</div>
		<!-- main-content closed -->
@endsection
@section('js')
{!! $validator->selector('#formCompany') !!}
<!--Internal  Select2 js -->
<script src="{{URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<!-- Internal Jquery.steps js -->
<script src="{{URL::asset('assets/plugins/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<!--Internal  Form-wizard js -->
<script src="{{URL::asset('assets/js/form-wizard.js')}}"></script>
@endsection
