@extends('layouts.master')
@section('css')
    <!-- Internal Nice-select css  -->
    <link href="{{ URL::asset('assets/plugins/jquery-nice-select/css/nice-select.css') }}" rel="stylesheet" />
@section('title')
    تعديل شركات
@stop


@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">الشركات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل
                شركة</span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
@if (session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<!-- row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary btn-sm" href="{{ route('company.allcompany') }}">رجوع</a>
                    </div>
                </div><br>

                <form action="{{ route('company.updateCompany') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="id" value="{{ $company->id }}">
                    {{-- 1 --}}

                        <div class="control-group form-group">
                            <label for="inputName" class="control-label">اسم شركة </label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ $company->name }}">
                        </div>



                        <div class="control-group form-group">
                            <label for="inputName" class="control-label"> ايميل الشركة </label>
                            <input type="text" class="form-control" id="email" name="email"
                                value="{{ $company->email }}">
                        </div>



                        <div class="control-group form-group">
                            <label for="inputName" class="control-label"> رقم الهاتف </label>
                            <input type="text" class="form-control" id="phone" name="phone"
                                value="{{ $company->phone }}">
                        </div>



                        <div class="control-group form-group">
                            <label for="inputName" class="control-label"> العنوان </label>
                            <input type="text" class="form-control" id="location" name="location"
                                value="{{ $company->location }}">
                        </div>



                        <div class="control-group form-group">
                            <label for="inputName" class="control-label"> رقم الشركة المرجعي </label>
                            <input type="text" class="form-control" id="ref_num" name="ref_num"
                                value="{{ $company->ref_num }}">
                        </div>



                        <div class="control-group form-group">
                            <label for="inputName" class="control-label"> معلومات اكثر عن شركة </label>
                            <input type="text" class="form-control" id="details" name="details"
                                value="{{ $company->details }}">
                        </div>





                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-success">حفظ البيانات</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>




</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')

<!-- Internal Nice-select js-->
<script src="{{ URL::asset('assets/plugins/jquery-nice-select/js/jquery.nice-select.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/jquery-nice-select/js/nice-select.js') }}"></script>

<!--Internal  Parsley.min js -->
<script src="{{ URL::asset('assets/plugins/parsleyjs/parsley.min.js') }}"></script>
<!-- Internal Form-validation js -->
<script src="{{ URL::asset('assets/js/form-validation.js') }}"></script>
@endsection
