@extends('layouts.master')
@section('css')

@section('title')
    الدفتر المحاسبى
@stop

<!-- Internal Data table css -->

<link href="{{ URL::asset('assets/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/datatable/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/datatable/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/datatable/css/responsive.dataTables.min.css') }}" rel="stylesheet">
<!--Internal   Notify -->
<link href="{{ URL::asset('assets/plugins/notify/css/notifIt.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">

<!-- Internal Spectrum-colorpicker css -->
<link href="{{ URL::asset('assets/plugins/spectrum-colorpicker/spectrum.css') }}" rel="stylesheet">

<!-- Internal Select2 css -->
<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">

@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">الدفتر المحاسبى</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ قائمه الدفاتر المحاسبيه </span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection

@section('content')

@if (session()->has('delete_AccountingNotebook'))
        <script>
            window.onload = function() {
                notif({
                    msg: "تم حذف القيد المحاسبى بنجاح",
                    type: "success"
                })
            }

        </script>
@endif

<!-- row opened -->
<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="col-sm-1 col-md-2">
<a class="btn btn-primary btn-sm" href="{{ route('addmyaccountants', $dftr_id) }}">اضافة قيد محاسبى</a>
<a class="btn btn-primary btn-sm" href="{{ route('myaccountants') }}">رجوع</a>

                </div>
                <div class="card-header pb-0">

                    <form action="#" method="POST" role="search" autocomplete="off">
                        {{ csrf_field() }}

                        <div class="col-lg-3">
                            <label class="rdiobox">
                                <input checked name="rdio" type="radio" value="1" id="type_div"> <span>بحث بتاريخ
                                    القيود</span></label>
                        </div><br>

                            <div class="col-lg-3" id="start_at">
                                <input type="date" name="search" id="search" onchange="search_in_accounting(this)" class="form-control">
                                </div>
                            </div>

                        </div><br>

                    </form>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive hoverable-table">

                    <table class="table table-hover" id="example1" data-page-length='50' style=" text-align: center;">
                        <thead>
                            <tr>
                                <th class="wd-10 border-bottom-0">#</th>
                                <th class="wd-10 border-bottom-0">القيد</th>
                                <th class="wd-10 border-bottom-0">رقم الفاتوره</th>
                                <th class="wd-10 border-bottom-0">رقم العرض</th>
                                <th class="wd-20 border-bottom-0" style="width: 40px">تاريخ القيد</th>
                                <th class="wd-10 border-bottom-0">مدين</th>
                                <th class="wd-10 border-bottom-0">دائن</th>
                                <th class="wd-10 border-bottom-0">شركات</th>
                                <th class="wd-10 border-bottom-0">الصندوق</th>
                                <th class="wd-10 border-bottom-0">جهة المرسل لها</th>
                                <th class="wd-15 border-bottom-0">حاله التصفيه</th>
                                <th class="wd-10 border-bottom-0">الرقم المرجعى</th>
                                <th class="wd-10 border-bottom-0" style="width: 50px">العمليات</th>
                                <th class="wd-10 border-bottom-0"  style="width: 95px">المرفقات</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if (isset($allacocountingnotbooks) && !empty($allacocountingnotbooks))
                                @foreach ($allacocountingnotbooks as $key => $allacocountingnotbook)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->Constraint }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->InvoiceNumber }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->display_number }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->DateOfRegistration }}</td>
                                        <td style="color:rgb(0, 255, 8)" >{{+ $allacocountingnotbook->accountting->Creditor }}</td>
                                        <td style="color:rgb(255, 0, 0)" >{{- $allacocountingnotbook->accountting->Debtor }}</td>
                                        <td style="color:rgb(25, 0, 255)">{{ $allacocountingnotbook->accountting->company->name }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->box->name }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->SendTo->name }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->LiquidationStatus->name }}</td>
                                        <td>{{ $allacocountingnotbook->accountting->ReferenceNumber }}</td>
                                        {{-- <td>{{ $AccountingNotebook->photo }}</td> --}}
                                        <td>
                                            <a href="{{ route('AccountingNotebook.edit' , $allacocountingnotbook) }}" class="btn btn-sm btn-info" title="تعديل">
                                                <i class="las la-pen"></i>
                                            </a>
                                            <a class="modal-effect btn btn-sm btn-danger" data-effect="effect-scale"
                                                data-invoice_id="{{ $allacocountingnotbook->accountting->id }}" data-boxname="{{ $allacocountingnotbook->accountting->name }}"
                                                data-toggle="modal" data-target="#delete_invoice" href="#modaldemo8" title="حذف">
                                                <i class="las la-trash"></i>
                                            </a>
                                        </td>

                                        <td>
                                            @if (!is_null($allacocountingnotbook->accountting->photo))

                                            <a class="btn btn-warning btn-sm" href="{{ url()->to('/') }}/AccountingNotebookImages/{{ $allacocountingnotbook->accountting->photo }}" target="_blank" download>
                                                <i class="fas fa-download"></i>
                                                &nbsp;
                                                تحميل</a>
                                            @endif

                                            @if (!is_null($allacocountingnotbook->accountting->photo))

                                            <a class="btn btn-success btn-sm"  href="{{ url()->to('/') }}/AccountingNotebookImages/{{ $allacocountingnotbook->accountting->photo }}" target="_blank">
                                                <i class="fas fa-eye"></i>
                                                &nbsp;
                                                عرض</a>
                                            @endif

                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--/div-->
    		 <!-- حذف الفاتورة -->
             <div class="modal fade" id="delete_invoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">حذف الدفتر المحاسبى</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>

                         @if (isset($allacocountingnotbook) && !empty($allacocountingnotbook))
                            <form action="{{ route('AccountingNotebook.delete' , $allacocountingnotbook->id) }}" method="GET">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                        @endif
                     </div>
                     <div class="modal-body">
                         هل انت متاكد من عملية الحذف ؟
                         <input type="hidden" name="id" id="invoice_id" value="">
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                         <button type="submit" class="btn btn-danger">تاكيد</button>
                     </div>
                     </form>
                 </div>
             </div>
         </div>

</div>

</div>
<!-- /row -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')
<!-- Internal Data tables -->
<script src="{{ URL::asset('assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/responsive.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/responsive.bootstrap4.min.js') }}"></script>
<!--Internal  Datatable js -->
<script src="{{ URL::asset('assets/js/table-data.js') }}"></script>
<!--Internal  Notify js -->
<script src="{{ URL::asset('assets/plugins/notify/js/notifIt.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/notify/js/notifit-custom.js') }}"></script>
<!-- Internal Modal js-->
<script src="{{ URL::asset('assets/js/modal.js') }}"></script>


<script>
    function search_in_accounting(element) {
        console.log(element.value);
        $.post("{{ route('search_in_accounting') }}",
        {
            _token: "{{ csrf_token() }}",
            search: element.value
        },
        function (data, status) {
            console.log(data);
            document.getElementById("example1").innerHTML = '';
            $("#example1").html(data);
        }
        );
    }
</script>

<script>
    $('#delete_invoice').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var invoice_id = button.data('invoice_id')
        var modal = $(this)
        modal.find('.modal-body #invoice_id').val(invoice_id);
    })

</script>




@endsection
