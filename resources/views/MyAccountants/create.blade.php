@extends('layouts.master')
@section('css')
    <!--- Internal Select2 css-->
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
    <!---Internal Fileupload css-->
    <link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css') }}" rel="stylesheet" type="text/css" />
    <!---Internal Fancy uploader css-->
    <link href="{{ URL::asset('assets/plugins/fancyuploder/fancy_fileupload.css') }}" rel="stylesheet" />
    <!--Internal Sumoselect css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/sumoselect/sumoselect-rtl.css') }}">
    <!--Internal  TelephoneInput css-->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/telephoneinput/telephoneinput-rtl.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
@section('title')
    اضافة قيد محاسبى
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">قيد  المحاسبى</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                اضافة قيد محاسبى</span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                {{-- < class="col-lg-12 margin-tb"> --}}
                    <div class="row">
                        <a class="btn btn-primary btn-sm" href="{{ route('allaccountingnotebook.show',$dftr_id) }}">رجوع</a>
                    </div><br>
                {{-- </div> --}}
                <form action="javascript: void(0);" class="parsley-style-1" id="selectForm2 AccountingNotebookForm"
                    autocomplete="off" name="selectForm2" enctype="multipart/form-data"
                    onsubmit="AccountingNotebookFormCreate(this)">
                    @csrf
                    <input type="hidden" name="myaccountants_id" id="myaccountants_id" value="{{ $dftr_id }}">
                    {{-- @method('GET') --}}
                    <div class="row">
                            <div class="col" id="fnWrapper">
                                <label>القيد: <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20"
                                    data-parsley-class-handler="#lnWrapper" name="Constraint" type="text">

                                <span class="text-danger" id="Constraint_error"></span>
                            </div>

                            <div class="col" id="fnWrapper">
                                <label>رقم الفاتوره : <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20"
                                    data-parsley-class-handler="#lnWrapper" name="InvoiceNumber" type="text">

                                <span class="text-danger" id="InvoiceNumber_error"></span>
                            </div>

                            <div class="col" id="fnWrapper">
                                <label>رقم العرض : <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20"
                                    data-parsley-class-handler="#lnWrapper" name="display_number" type="text">

                                <span class="text-danger" id="display_number_error"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" id="fnWrapper">
                                <label>مدين : <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20" style="color:rgb(0, 255, 8)"
                                    data-parsley-class-handler="#lnWrapper" name="Creditor" type="number">

                                <span class="text-danger" id="Creditor_error"></span>
                            </div>
                            <div class="col" id="fnWrapper">
                                <label>دائن : <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20" style="color:rgb(255, 0, 0)"
                                data-parsley-class-handler="#lnWrapper" name="Debtor" type="number">

                                <span class="text-danger" id="Debtor_error"></span>
                            </div>
                        </div>

                            <div class="row">
                            <div class="col" id="fnWrapper">
                                <label> الصندوق : <span class="tx-danger"></span></label>
                                @if (isset($boxs) && $boxs->count() >= 0)
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="Box_id">
                                        @foreach ($boxs as $box)
                                            <option value="{{ $box->id }}">{{ $box->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger" id="Box_id_error"></span>
                                @else
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="Box_id">
                                        <option disabled selected>لا يوجد اى بيانات</option>
                                    </select>
                                @endif
                                <span class="text-danger" id="name_error"></span>
                            </div>

                            <div class="col" id="fnWrapper">
                                <label> جهه المرسل لها : <span class="tx-danger"></span></label>
                                @if (isset($Addressees) && $Addressees->count() >= 0)
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="SendTo_id">
                                        @foreach ($Addressees as $SendTo)
                                            <option value="{{ $SendTo->id }}">{{ $SendTo->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger" id="SendTo_id_error"></span>
                                @else
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="SendTo_id">
                                        <option disabled selected>لا يوجد اى بيانات</option>
                                    </select>
                                @endif
                                <span class="text-danger" id="name_error"></span>
                            </div>
                            <div class="col" id="fnWrapper">
                                <label> حاله التصفيه : <span class="tx-danger"></span></label>
                                @if (isset($LiquidationCases) && $LiquidationCases->count() >= 0)
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="LiquidationStatus_id">
                                        @foreach ($LiquidationCases as $LiquidationCase)
                                            <option value="{{ $LiquidationCase->id }}">{{ $LiquidationCase->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger" id="LiquidationStatus_id_error"></span>
                                @else
                                    <select class="form-control form-control-sm mg-b-20"
                                        aria-label="Default select example" name="LiquidationStatus_id">
                                        <option disabled selected>لا يوجد اى بيانات</option>
                                    </select>
                                @endif
                                <span class="text-danger" id="name_error"></span>
                            </div>

                            <div class="col" id="fnWrapper">
                                <label>الرقم المرجعى : <span class="tx-danger"></span></label>
                                <input class="form-control form-control-sm mg-b-20"
                                    data-parsley-class-handler="#lnWrapper" name="ReferenceNumber" type="text">

                                <span class="text-danger" id="ReferenceNumber_error"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label>تاريخ القيد : </label>
                                <input class="form-control" name="DateOfRegistration"
                                    placeholder="YYYY-MM-DD" type="date" value="{{ date('Y-m-d') }}" required>

                                <span class="text-danger" id="DateOfRegistration_error"></span>
                            </div>

                                <div class="col" id="fnWrapper">
                                    <label> شركة :  <span class="tx-danger"></span></label>
                                    @if (isset($companyes) && $companyes->count() >= 0)
                                        <select id="companies_id" class="js-choice"
                                        name="companies_id" placeholder="Native Select"
                                        data-search="false" data-silent-initial-value-set="true">
                                            @foreach ($companyes as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger" id="companies_id_error"></span>
                                    @else
                                        <select class="form-control form-control-sm mg-b-20" aria-label="Default select example" name="companies_id">
                                                <option disabled selected>لا يوجد اى بيانات</option>
                                        </select>
                                    @endif
                                    <span class="text-danger" id="name_error"></span>
                                </div>
                            </div>


                        <p class="text-danger">* صيغة المرفق pdf, jpeg ,.jpg , png </p>
                        <h5 class="card-title">المرفقات</h5>

                        <div class="col-sm-12 col-md-12">
                            <input type="file" name="photo" class="dropify" accept=".pdf,.jpg, .png, image/jpeg, image/png"
                                data-height="70" />
                        </div><br>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary">حفظ البيانات</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')


    <!-- Internal Select2 js-->
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <!--Internal Fileuploads js-->
    <script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
    <!--Internal Fancy uploader js-->
    <script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.ui.widget.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.fileupload.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.iframe-transport.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.fancy-fileupload.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/fancyuploder/fancy-uploader.js') }}"></script>
    <!--Internal  Form-elements js-->
    <script src="{{ URL::asset('assets/js/advanced-form-elements.js') }}"></script>
    <script src="{{ URL::asset('assets/js/select2.js') }}"></script>
    <!--Internal Sumoselect js-->
    <script src="{{ URL::asset('assets/plugins/sumoselect/jquery.sumoselect.js') }}"></script>
    <!--Internal  Datepicker js -->
    <script src="{{ URL::asset('assets/plugins/jquery-ui/ui/widgets/datepicker.js') }}"></script>
    <!--Internal  jquery.maskedinput js -->
    <script src="{{ URL::asset('assets/plugins/jquery.maskedinput/jquery.maskedinput.js') }}"></script>
    <!--Internal  spectrum-colorpicker js -->
    <script src="{{ URL::asset('assets/plugins/spectrum-colorpicker/spectrum.js') }}"></script>
    <!-- Internal form-elements js -->
    <script src="{{ URL::asset('assets/js/form-elements.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/noty@3/lib/noty.css">
<script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>

<script>
    const element = document.querySelector('.js-choice');
    const choices = new Choices(element);
</script>


<script>
    function AccountingNotebookFormCreate(element) {
        var form = new FormData(element);
        $.ajax({
            type: 'POST',
            enctype: "multipart/form-data",
            url: "{{ route('AccountingNotebook.store') }}",
            cache: false,
            contentType: false,
            processData: false,
            data: form,
            success: function(data) {
                if (data.status == true) {
                    $('#msg_AccountingNotebook').show();
                    $("#notAdded").hide();
                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "{{ 'تمت عمليه الاضافه بنجاح' }}",
                        timeout: 2000,
                        killer: true
                    }).show();
                    window.location=document.referrer;
                    // window.history.go(-1);
                } else {
                    $('#msg_AccountingNotebook').show();
                    $("#notAdded").hide();
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: "{{ 'حدث خطأ ما . يرجى اعاده المحاوله فى وقت لاحق ' }}",
                        timeout: 2000,
                        killer: true
                    }).show();
                }
            },
            error: function(errorMessages) {

                var messages = JSON.parse(errorMessages.responseText);

                $.each(messages.errors, function(key, val) {

                    key = key.replace(/[.]/g, '_');
                    $("#" + key + "_error").text(val); // It shows product name errors

                });
            }
        });
    }
</script>

@endsection
