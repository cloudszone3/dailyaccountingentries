@extends('layouts.master')
@section('css')

@section('title')
    الدفاتر المحاسبية
@stop

<!-- Internal Data table css -->

<link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
<!---Internal Fileupload css-->
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css') }}" rel="stylesheet" type="text/css" />
<!---Internal Fancy uploader css-->
<link href="{{ URL::asset('assets/plugins/fancyuploder/fancy_fileupload.css') }}" rel="stylesheet" />
<!--Internal Sumoselect css-->
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/sumoselect/sumoselect-rtl.css') }}">
<!--Internal  TelephoneInput css-->
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/telephoneinput/telephoneinput-rtl.css') }}">

@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">الدفاتر المحاسبية</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                قائمه الدفاتر المحاسبيه </span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection

@section('content')

<!-- row opened -->
<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="col-sm-1 col-md-2">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                        اضافة دفتر محاسبي
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">أضافة دفتر محاسبي</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
{{-- انشاء دفتر ححاسبي --}}
                                <form id="formInvoice" action="{{ route('myaccountants.store') }}" method="post"
                                    enctype="multipart/form-data" style="width: 100%;">
                                    <div class="modal-body">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col">
                                                <label for="inputName" class="form-label">رقم الدفتر</label>
                                                <input type="number" class="form-control" id="inputName"
                                                    name="accountant_number">
                                            </div>

                                            <div class="col">
                                                <label class="form-label">اسم الدفتر</label>
                                                <input type="text" class="form-control required"
                                                    name="accountant_name">
                                            </div>


                                            <div class="col">
                                                <label>تاريخ الدفتر </label>
                                                <input class="form-control" name="date" id="date"
                                                    placeholder="YYYY-MM-DD" type="date" value="{{ date('Y-m-d') }}">
                                            </div>
                                        </div>
                                        <div class="ro">
                                            <div class="col">
                                                <label for="exampleTextarea">ملاحظات</label>
                                                <textarea class="form-control" id="exampleTextarea" name="note" rows="3"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">اغلاق</button>
                                        <button type="submit" class="btn btn-success">حفظ البيانات</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    </div>

            <div class="card-body">
                <div class="table-responsive hoverable-table">

                    <table class="table table-hover" id="example1"  style=" text-align: center; width: 100%;">
                        <thead>
                            <tr>
                                <th class="border-bottom-0">#</th>
                                <th class="border-bottom-0">رقم الدفتر</th>
                                <th class="border-bottom-0">اسم الدفتر</th>
                                <th class="border-bottom-0">تاريخ الدفتر</th>
                                <th class="border-bottom-0">ملاحظات</th>
                                <th class="border-bottom-0">العمليات</th>
                            </tr>
                        </thead>
                        <tbody>

                             @if (isset($myaccountants) && !empty($myaccountants))
                                @foreach ($myaccountants as $key => $myaccountant)
                                    <tr>
                                        <td>{{ $key+1}}</td>
                                        <td>{{ $myaccountant->accountant_number }}</td>
                                        <td>{{ $myaccountant->accountant_name }}</td>
                                        <td>{{ $myaccountant->date }}</td>
                                        <td>{{ Illuminate\Support\Str::limit($myaccountant->note, 30, $end = '...') }}</td>

                                        <td>

                                            <a href="{{ route('myaccountants.edit' , $myaccountant->id) }}" class="btn btn-sm btn-info" title="تعديل">
                                                <i class="las la-pen"></i>
                                            </a>

                                            <a href="{{ route('myaccountants.delete' , $myaccountant->id) }}" class="modal-effect btn btn-sm btn-danger" title="حذف">
                                                <i class="las la-trash"></i>
                                            </a>

                                            {{-- <a href="{{ route('addmyaccountants' , $myaccountant->id) }}" class="btn btn-secondary btn-sm" title="اضافة قيد">
                                                <i class="las la-plus"></i>
                                            </a> --}}

                                            <a href="{{ route('allaccountingnotebook.show' , $myaccountant->id) }}" class="btn btn-success btn-sm" title="عرض القيود">
                                                <i class="fas fa-eye"></i>
                                            </a>

                                        </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

</div>
<!-- /row -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection
@section('js')

{!! $validator->selector('#formInvoice') !!}
<!-- Internal Select2 js-->
<script src="{{ URL::asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<!--Internal Fileuploads js-->
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
<!--Internal Fancy uploader js-->
<script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.ui.widget.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.iframe-transport.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fancyuploder/jquery.fancy-fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fancyuploder/fancy-uploader.js') }}"></script>
<!--Internal  Form-elements js-->
<script src="{{ URL::asset('assets/js/advanced-form-elements.js') }}"></script>
<script src="{{ URL::asset('assets/js/select2.js') }}"></script>
<!--Internal Sumoselect js-->
<script src="{{ URL::asset('assets/plugins/sumoselect/jquery.sumoselect.js') }}"></script>
<!--Internal  Datepicker js -->
<script src="{{ URL::asset('assets/plugins/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<!--Internal  jquery.maskedinput js -->
<script src="{{ URL::asset('assets/plugins/jquery.maskedinput/jquery.maskedinput.js') }}"></script>
<!--Internal  spectrum-colorpicker js -->
<script src="{{ URL::asset('assets/plugins/spectrum-colorpicker/spectrum.js') }}"></script>
<!-- Internal form-elements js -->
<script src="{{ URL::asset('assets/js/form-elements.js') }}"></script>

@endsection
